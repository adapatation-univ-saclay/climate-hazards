FROM alpine:3.16.9

WORKDIR /code

# install build dependencies
RUN apk add --no-cache hdf5-dev

# Geographic data libraries
RUN apk add proj-util proj-dev gcc musl-dev
RUN export PROJ_DIR=/usr/bin/proj
RUN export PROJ_INCDIR=/usr/include/proj

# NetCDF files
RUN apk add netcdf-dev
# PostgreSQL
RUN apk add libpq-dev
# For rust dependencies
RUN apk add git
# Numpy
RUN apk add g++

# To download the rust toolchain and pyenv
RUN apk add curl
# To build hdf5 from source
RUN apk add make cmake

# Install pyenv
RUN apk add bash
RUN apk add libffi-dev bzip2-dev ncurses-dev openssl-dev readline-dev tk-dev xz-dev zlib-dev
RUN curl https://pyenv.run | bash

COPY create_pyenv.sh /code/create_pyenv.sh
RUN chmod +x /code/create_pyenv.sh
RUN ./create_pyenv.sh

# Install rust toolchain
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
#RUN source "$HOME/.cargo/env"

# Install python requirements
COPY ./requirements.txt /code/requirements.txt
#RUN source .venv/bin/activate
RUN .venv/bin/pip install --no-cache-dir --upgrade -r /code/requirements.txt

# copy code
COPY ./res /code/res
COPY ./app /code/app
COPY ./api /code/api
COPY ./config.py /code/config.py
COPY ./setup.sh /code/setup.sh

RUN chmod +x setup.sh
RUN ./setup.sh

# copy env variables
COPY ./.env /code/.env

CMD [".venv/bin/uvicorn", "api.routes:api", "--proxy-headers", "--host", "0.0.0.0", "--port", "8080"]
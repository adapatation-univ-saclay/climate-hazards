def _next_level(depth: int):
    return max(depth - 1, 0)


def serialize(obj: list | dict | object, depth: int = 0, base_url: str | None = None):
    """Serialize the given object, recursively for a list/dict/set

    Args:
        obj: the object or collection to serialize
        depth: the depth of serialization to apply inside the object
        request: the request to add urls to serialized object
    """
    match obj:
        case str():
            return obj
        case list():
            return [
                serialize(element, depth=_next_level(depth), base_url=base_url)
                for element in obj
            ]
        case dict():
            return {
                key: serialize(val, depth=_next_level(depth), base_url=base_url)
                for (key, val) in obj.items()
            }
        case set():
            return {
                serialize(val, depth=_next_level(depth), base_url=base_url)
                for val in obj
            }
        case _:
            return obj.serialize(depth=_next_level(depth), base_url=base_url)

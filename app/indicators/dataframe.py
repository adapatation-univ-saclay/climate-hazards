from __future__ import annotations

import warnings as _warnings
from collections.abc import Sequence as _Sequence
from typing import Type as _Type

import pandas as _pd
import pint as _pint
import pint_pandas
import plotly.express as _px
import plotly.graph_objects as go
from pandas._typing import Axes as _Axes
from pandas._typing import Dtype as _Dtype

from app.locale import _
from app.models.model import Model as _Model
from app.models.models import parse_model as _parse_model
from app.scenarios.parser import parse as _parse_scenario
from app.scenarios.scenario import Scenario as _Scenario
from app.units import Unit
from app.units.names import friendly_name


class IndicatorDataFrame(_pd.DataFrame):
    # DataFrame columns that do not represent indicators
    NON_INDICATORS = set(["month", "year", "scenario", "model", "date"])

    @property
    def _constructor(self):
        def f(*args, **kwargs):
            # allow unit transmission in filtered DataFrames
            return IndicatorDataFrame(*args, **kwargs, unit=self.unit)

        return f

    def __init__(
        self,
        data=None,
        index: _Axes | None = None,
        columns: _Axes | None = None,
        dtype: _Dtype | None = None,
        copy: bool | None = None,
        unit: Unit | None = None,
    ):
        _pd.DataFrame.__init__(self, data, index, columns, dtype, copy)
        compute_date = lambda date: f'{date["year"]}-{date["month"]}'

        if len(self) > 0:
            self["date"] = self.apply(compute_date, axis=1)

        self.unit = unit
        self["value"] = self["value"].astype(f"pint[{str(unit)}]")

    def display_unit(self) -> str:
        return friendly_name(self.unit)

    def convert(self, unit: str | Unit) -> IndicatorDataFrame:
        """Perform unit conversion

        Args:
            unit: the unit to convert this dataframe to
        """
        df = self.copy()
        df["value"] = df["value"].pint.to(unit)
        df.unit = unit
        return df

    def take_month(self, month: int) -> IndicatorDataFrame:
        """Returns an IndicatorDataFrame that only contains rows corresponding to
        the select month"""
        return self[self["month"] == month]

    def take_scenario(
        self, scenario: _Scenario | _Sequence[_Scenario]
    ) -> IndicatorDataFrame:
        """Returns an IndicatorDataFrame that only contains rows corresponding to
        the select scenario(s)"""
        if isinstance(scenario, _Scenario):
            return self[self["scenario"] == scenario]
        else:
            return self[self["scenario"].isin(scenario)]

    def variables_names(self) -> list[str]:
        """Return the list of all different variables contained in this DataFrame"""
        return list(set(self.columns) - IndicatorDataFrame.NON_INDICATORS)

    def scenarios(self) -> list[_Scenario]:
        """Return the list of all scenarios that this DataFrame contains."""
        scens = list(self.scenario.unique())

        return scens

    def models(self) -> list[_Model]:
        """Return the list of all models that this DataFrame contains."""
        models = list(self.model.unique())

        return models

    def indicators(self) -> list["Indicator"]:
        """Return the list of all indicators that this DataFrame contains."""
        return list(self.indicator.unique())

    def plot(
        self, kind: str = "all_models", to: str = "display", path: str | None = None
    ):
        # TODO: plot kinds
        # all_models
        # minmax
        var1 = "value"

        indicators = self.indicators()

        if len(indicators) == 0:
            return go.Figure(layout=dict(title=dict(text=_("interface.no_data"))))

        for indicator in indicators:

            fig = go.Figure(
                layout=dict(
                    title=dict(
                        text=f"{_('interface.projection_of')} {indicator.locale_name()} ({self.display_unit()})"
                    )
                )
            )
            for scenario in self.scenarios():
                scenario_df = self[self["scenario"] == scenario].sort_values(
                    by=["model", "scenario", "year", "month"]
                )
                fill = {}
                for model in self.models():
                    df = scenario_df[scenario_df["model"] == model]

                    with _warnings.catch_warnings():
                        _warnings.filterwarnings(
                            "ignore", category=_pint.errors.UnitStrippedWarning
                        )
                        quantity = df[var1].values.quantity

                        legend_group = dict(
                            legendgroup=scenario.name,
                            legendgrouptitle=dict(text=scenario.name),
                        )

                        fig.add_trace(
                            go.Scatter(
                                x=df["date"],
                                y=quantity,
                                mode="lines",
                                name=f"{model.name} – {scenario.name}",
                                line_color=scenario.color,
                                **legend_group,
                            )
                        )

                sorted_df = scenario_df.sort_values(["date", var1])

                min_df = sorted_df.groupby(["date"]).head(1)
                max_df = sorted_df.groupby(["date"]).tail(1)

                with _warnings.catch_warnings():
                    _warnings.filterwarnings(
                        "ignore", category=_pint.errors.UnitStrippedWarning
                    )
                    min_quantity = min_df[var1].values.quantity
                    max_quantity = max_df[var1].values.quantity

                    fig.add_trace(
                        go.Scatter(
                            x=min_df["date"],
                            y=min_quantity,
                            mode="lines",
                            name=f"minimum – {scenario.name}",
                            line_color=scenario.color,
                            **legend_group,
                        )
                    )

                    fig.add_trace(
                        go.Scatter(
                            x=max_df["date"],
                            y=max_quantity,
                            mode="lines",
                            name=f"maximum – {scenario.name}",
                            fill="tonexty",
                            line_color=scenario.color,
                            **legend_group,
                        )
                    )

                fig.update_layout(legend_groupclick="toggleitem")

        match to:
            case "html":
                fig.write_html(path)
            case "dash":
                return fig
            case _:
                fig.show()

        # fig = _px.line(
        #    df,
        #    x="date",
        #    y=var1,
        #    title=f"Projection of {var1} over time {self.display_unit()}",
        #    color="scenario",
        #    symbol="model",
        # )

    def __add__(self, other) -> IndicatorDataFrame:
        if isinstance(other, IndicatorDataFrame):
            return IndicatorDataFrame(
                self.to_dict("records") + other.to_dict("records")
            )
        elif isinstance(other, int) or isinstance(other, float):
            records = self.to_dict("records")
            var_names = self.variables_names()
            for row in records:
                for var in var_names:
                    row[var] += other
            return IndicatorDataFrame(records)
        else:
            raise TypeError(
                f"Cannot add IndicatorDataFrame to object of type {type(other)}"
            )

    def __sub__(self, other) -> IndicatorDataFrame:
        if isinstance(other, int) or isinstance(other, float):
            return self + (-other)
        else:
            raise TypeError(
                f"Cannot subtract object of type {type(other)} from an IndicatorDataFrame"
            )

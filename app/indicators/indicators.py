from app.datasets.datasets import CMIP6_PROJECTIONS as _CMIP6_PROJECTIONS
from app.hazards.heat import mean_air_temperature
from app.indicators.indicator import Indicator
from app.time.period import Day
from app.units.temperature import Kelvin

DAILY_AIR_TEMPERATURE = Indicator(
    name="Daily surface temperature - CMIP6 projections",
    dataset=_CMIP6_PROJECTIONS,
    hazard=mean_air_temperature,
    variable_name="daily_maximum_near_surface_air_temperature",
    temporal_resolution=Day,
    unit=Kelvin,
)

all_indicators = [DAILY_AIR_TEMPERATURE]

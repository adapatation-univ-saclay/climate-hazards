import glob
import math
import os
import shutil
from collections.abc import Iterable
from collections.abc import Sequence as _Sequence
from datetime import datetime
from enum import Enum, StrEnum
from itertools import product
from zipfile import ZipFile

import cdsapi as _cdsapi
import numpy as np
import pandas as pd
import psycopg2
import xarray as xr
from cdo import Cdo
from dateutil import parser as datetime_parser
from nc_pgstore import store_netcdf_file_to_postgis
from netCDF4 import Dataset as NetCDFFile
from numpy import argmin

from app.database.handler import DBHandler
from app.datasets.dataset import Dataset as _Dataset
from app.hazards.hazard import Hazard as _Hazard
from app.indicators.dataframe import IndicatorDataFrame
from app.locale import _
from app.models import Model as _Model
from app.models.models import all_models, parse_model
from app.regions import Region as _Region
from app.regions.geometry import Point
from app.scenarios.parser import parse as parse_scenario
from app.scenarios.scenario import Scenario as _Scenario
from app.time.period import Period as _Period
from app.units import Q_, Unit
from config import BASE_DATA_DIR
from config import DATABASE as DATABASE_CONFIG


class Method(StrEnum):
    AVERAGE = "average"
    MIN = "minimum"
    MAX = "maximum"


class Status(Enum):
    NOT_DOWNLOADED = 0
    DOWNLOADED = 1
    DOWNSCALED = 2

    UNAVAILABLE = -99


class Indicator:
    def __init__(
        self,
        name: str,
        dataset: _Dataset,
        hazard: _Hazard,
        variable_name: str,
        temporal_resolution: _Period,
        unit: Unit,
    ):
        self.name = name
        self.dataset = dataset
        self.hazard = hazard
        self.variable_name = variable_name
        self.temporal_resolution = temporal_resolution
        self.unit = unit

    @property
    def name(self) -> str:
        """The full name of the indicator"""
        return self._name

    @name.setter
    def name(self, name: str):
        self._name = name

    def directory_name(self) -> str:
        """The name of the indicator with every separator replaced by underscore,
        and lowercased for directory compatibility"""
        return (
            self.name.lower()
            .replace(".", "_")
            .replace("-", "_")
            .replace("/", "_")
            .replace(" ", "_")
        )

    @property
    def dataset(self) -> _Dataset:
        """The name of the copernicus dataset"""
        return self._dataset

    @dataset.setter
    def dataset(self, dataset: _Dataset):
        self._dataset = dataset

    @property
    def variable_name(self) -> str:
        """The copernicus name of the indicator's variable"""
        return self._variable_name

    @variable_name.setter
    def variable_name(self, variable_name: str):
        self._variable_name = variable_name

    @property
    def resolution(self) -> Q_:
        """The resolution of the indicator"""
        return self._resolution

    @resolution.setter
    def resolution(self, resolution: Q_):
        self._resolution = resolution

    @property
    def unit(self) -> Unit:
        """The unit of indicator values"""
        return self._unit

    @unit.setter
    def unit(self, unit: Unit):
        self._unit = unit

    @property
    def hazard(self) -> _Hazard:
        """The hazard whose evolution this indicator represents"""
        return self._hazard

    @hazard.setter
    def hazard(self, hazard: _Hazard):
        self._hazard = hazard

    @property
    def id(self) -> int:
        """The indicator id in database"""
        return self._id

    @id.setter
    def id(self, id: int):
        self._id = id

    def serialize(self, depth: int = 1):
        return {"name": self.name, "temporal_resolution": self.temporal_resolution}

    def auto_period(self, scenario, period: _Period | None):
        if period is not None:
            return period
        return self.dataset.available_periods[scenario.name]

    def make_download_request_dict(
        self,
        scenario: _Scenario,
        region: _Region,
        model: _Model,
        period: _Period | None = None,
    ):
        """Returns the json request to give to the CDS API to download the indicator.

        Args:
            scenario: the CMIP/CORDEX scenario to use (e.g. app.scenarios.cmip6.SSP_2_4_4)
            region: the sub-region over which the indicator should be downloaded
            model: the chosen model to download
            period: the period over which the indicator should be downloaded. If None,
                will be set to the largest period available.
        """
        period = self.auto_period(scenario, period)

        as_dict = {
            "temporal_resolution": self.temporal_resolution.copernicus_resolution,
            "experiment": scenario.to_copernicus_name(),
            "variable": self.variable_name,
            "format": "zip",
            "model": model.to_copernicus_name(),
        }
        as_dict |= period.to_compact_dict_resolution(self.temporal_resolution)

        if region.bbox:
            bbox = region.bbox
            as_dict["area"] = [bbox.north, bbox.west, bbox.south, bbox.east]

        return as_dict

    def file_path(
        self,
        scenario: _Scenario,
        region: _Region,
        model: _Model,
        period: _Period | None = None,
        file_format: str | None = None,
        suffix: str | None = None,
    ):
        """Returns the path to the files associated with this indicator.

        Args:
            scenario: the CMIP/CORDEX scenario to use (e.g. app.scenarios.cmip6.SSP_2_4_4)
            region: the sub-region over which the indicator has been downloaded
            model: the model-specific instance of the indicator that has been downloaded
            period: the period over which the indicator has been downloaded. If None,
                will be set to the largest period available.
        """
        period = self.auto_period(scenario, period)

        suffix = f"_{suffix}" if suffix is not None else ""

        fmt = f".{file_format}" if file_format else ""
        return os.path.join(
            BASE_DATA_DIR,
            self.directory_name(),
            region.name,
            scenario.name,
            model.name,
            period.describe() + suffix + fmt,
        )

    def download_single(
        self,
        scenario: _Scenario,
        region: _Region,
        model: _Model,
        period: _Period | None = None,
    ):
        """Downloads a single-scenario, single-region, single-period, single-model
        instance of an indicator. Then extracts and rename files following a
        systematic name scheme.

        Args:
            scenario: the CMIP/CORDEX scenario to use (e.g. app.scenarios.cmip6.SSP_2_4_4)
            region: a region over which the indicator should be downloaded
            model: the chosen model to download
            period: the period over which the indicator should be downloaded. If None,
                will be set to the largest period available.

        Returns:
            the path to the NetCDF file
        """
        period = self.auto_period(scenario, period)

        request_dict = self.make_download_request_dict(
            scenario=scenario, region=region, period=period, model=model
        )

        cds = _cdsapi.Client()

        zip_path = self.file_path(
            scenario=scenario,
            region=region,
            model=model,
            period=period,
            file_format="zip",
        )
        dir_path = self.file_path(
            scenario=scenario, region=region, period=period, model=model
        )

        nc_path = self.file_path(
            scenario=scenario,
            region=region,
            model=model,
            period=period,
            file_format="nc",
        )

        if not os.path.exists(dir_path):
            os.makedirs(dir_path)

        try:
            cds.retrieve(self.dataset.name, request_dict, zip_path)
        except ValueError as e:
            os.remove(dir_path)
            raise e

        # extract zip file
        with ZipFile(zip_path, "r") as zip_file:
            zip_file.extractall(dir_path)

        # remove zip file
        os.remove(zip_path)

        # rename extracted netcdf file
        for file in os.listdir(dir_path):
            if file.endswith(".nc"):
                os.rename(os.path.join(dir_path, file), nc_path)

        return nc_path

    def download_bulk(
        self,
        scenarios: _Scenario | Iterable[_Scenario],
        regions: _Region | Iterable[_Region],
        models: _Model | Iterable[_Model],
        periods: _Period | Iterable[_Period],
    ):
        """Downloads multiple instances of an indicator.

        Args:
            scenario: a CMIP/CORDEX scenario, or an iterable of scenarios to use
                (e.g. app.scenarios.cmip6.SSP_2_4_4)
            region: a region, or iterable of regions over which the indicator should be downloaded
            model: a model, r iterable of models to download
            period: the period or periods over which the indicator should be downloaded. If None,
                will be set to the largest period available.

        Returns:
            the paths to all downladed files"""
        if isinstance(scenarios, _Scenario):
            scenarios = [scenarios]

        if isinstance(regions, _Region):
            regions = [regions]

        if isinstance(periods, _Period):
            periods = [periods]
        elif periods is None:
            periods = [None]

        if isinstance(models, _Model):
            models = [models]

        paths = []

        for scenario, region, period, model in product(
            scenarios, regions, periods, models
        ):
            paths.append(
                self.download_single(
                    scenario=scenario, region=region, period=period, model=model
                )
            )
        return paths

    def download(
        self,
        scenario: _Scenario | Iterable[_Scenario] | None = None,
        region: _Region | Iterable[_Region] | None = None,
        period: _Period | Iterable[_Period] | None = None,
        model: _Model | Iterable[_Model] | None = None,
        scenarios: _Scenario | Iterable[_Scenario] | None = None,
        regions: _Region | Iterable[_Region] | None = None,
        periods: _Period | Iterable[_Period] | None = None,
        models: _Model | Iterable[_Model] | None = None,
    ):
        var_or_iter = lambda var, iterbl: [var] if var is not None else iterbl

        scenarios = var_or_iter(scenario, scenarios)
        regions = var_or_iter(region, regions)
        periods = var_or_iter(period, periods)
        models = var_or_iter(model, models)

        return self.download_bulk(
            scenarios=scenarios,
            regions=regions,
            periods=periods,
            models=models,
        )

    def store(
        self,
        scenario: _Scenario,
        region: _Region,
        model: _Model,
        period: _Period | None = None,
    ):
        nc_path = self.file_path(
            scenario=scenario,
            region=region,
            model=model,
            period=period,
            file_format="nc",
        )

        store_netcdf_file_to_postgis(
            file_path=nc_path,
            connection_args=DATABASE_CONFIG,
            batch_size=1000,
            schema="indicator",
            table="values",
            indicator_id=self.id,
            scenario_id=scenario.id,
            model_id=model.id,
        )

    def convert_calendar(
        self,
        scenario: _Scenario,
        region: _Region,
        model: _Model,
        period: _Period | None = None,
    ):
        """Set the calendar type to gregorian when it is not the case, in order
        to avoid shifts in the date in further computations (which may happen
        for models using the 360-day calendar).

        Args:
            scenario: the scenario to convert
            region: the region that has been downloaded
            model: the model to convert
            period: the period of time that has been downloaded (defaults
                to the longest available period)"""
        nc_file_params = {
            "scenario": scenario,
            "region": region,
            "model": model,
            "period": period,
            "file_format": "nc",
        }

        base_file = self.file_path(**nc_file_params)
        tmp_file = self.file_path(**nc_file_params, suffix="tmp")
        tmp2_file = self.file_path(**nc_file_params, suffix="tmp2")
        tmp2_file = self.file_path(**nc_file_params, suffix="tmp2")
        nc = NetCDFFile(base_file)
        ds = xr.open_dataset(base_file)

        init_date = ds.time.values[0]
        if isinstance(init_date, np.datetime64):
            item = init_date.item()
            if isinstance(item, datetime):
                init_date = item
            else:
                init_date = datetime_parser.parse(str(init_date))

        year = str(init_date.year).zfill(4)
        month = str(init_date.month).zfill(2)
        day = str(init_date.day).zfill(2)
        hour = str(init_date.hour).zfill(2)
        minute = str(init_date.minute).zfill(2)
        second = str(init_date.second).zfill(2)

        init_date_str = f"{year}-{month}-{day},{hour}:{minute}:{second}"

        calendar_type = nc.variables["time"].calendar

        if calendar_type != "gregorian":
            cdo = Cdo()
            cdo.setreftime(
                "1850-01-01,00:00:00", "1day", input=base_file, output=tmp_file
            )
            cdo.settaxis(init_date_str, "1day", input=tmp_file, output=tmp2_file)
            os.remove(tmp_file)
            cdo.setcalendar("gregorian", input=tmp2_file, output=tmp_file)

            os.remove(base_file)
            os.rename(tmp_file, base_file)

    def daily_to_monthly(
        self,
        scenario: _Scenario,
        region: _Region,
        model: _Model,
        period: _Period | None = None,
        method: Method | str = Method.AVERAGE,
    ):
        period = self.auto_period(scenario, period)

        cdo = Cdo()

        nc_file_params = {
            "scenario": scenario,
            "region": region,
            "model": model,
            "period": period,
            "file_format": "nc",
        }

        base_file = self.file_path(**nc_file_params)
        backup_file = self.file_path(**nc_file_params, suffix="backup")

        if not os.path.exists(backup_file):
            print("Creating backup")
            shutil.copyfile(base_file, backup_file)

        monthly_file = self.file_path(**nc_file_params, suffix="monthly")
        daily_file = self.file_path(**nc_file_params, suffix="daily")
        tmp_file = self.file_path(**nc_file_params, suffix="tmp")

        match method:
            case "minimum" | "min":
                operator = cdo.monmin
            case "maximum" | "max":
                operator = cdo.monmax
            case "average" | "avg":
                operator = cdo.monavg
            case _:
                raise ValueError(
                    f"Unknown daily to monthly aggreation method {repr(method)}"
                )

        if self.temporal_resolution.copernicus_resolution == "daily":
            if not os.path.exists(daily_file):
                print("Renaming base file to daily")
                os.rename(base_file, daily_file)

            if not os.path.exists(monthly_file):
                print("Setting reference time")
                cdo.setreftime(
                    "1850-01-01,00:00:00",
                    "1day",
                    input=daily_file,
                    output=tmp_file,
                )

                print(f"Converting daily to monthly with method {method}")

                operator(input=tmp_file, output=monthly_file)

                print("Removing temporary file")

                os.remove(tmp_file)

    def remove_directory(
        self,
        scenario: _Scenario,
        region: _Region,
        model: _Model,
        period: _Period | None = None,
    ):

        base_file = self.file_path(
            scenario=scenario,
            region=region,
            model=model,
            period=period,
            file_format="nc",
        )

        directory = os.path.dirname(base_file)

        shutil.rmtree(directory)

    def store_monthly(
        self,
        scenario: _Scenario,
        region: _Region,
        model: _Model,
        period: _Period | None = None,
    ):
        nc_path = self.file_path(
            scenario=scenario,
            region=region,
            model=model,
            period=period,
            file_format="nc",
            suffix="monthly",
        )

        store_netcdf_file_to_postgis(
            file_path=nc_path,
            connection_args=DATABASE_CONFIG,
            batch_size=1000,
            schema="indicator",
            table="monthly_values",
            indicator_id=self.id,
            scenario_id=scenario.id,
            model_id=model.id,
        )

    def model_statuses(
        self, region: _Region, scenario: _Scenario, period: _Period | None = None
    ) -> list[_Model]:
        statuses = {}
        for model in all_models:
            file_params = {
                "scenario": scenario,
                "region": region,
                "model": model,
                "period": period,
            }
            period_directory_path = self.file_path(**file_params)
            directory_path = os.path.dirname(period_directory_path)

            if not os.path.exists(directory_path):
                statuses[model] = Status.NOT_DOWNLOADED

            else:
                content = [
                    os.path.join(directory_path, f) for f in os.listdir(directory_path)
                ]

                base_path = self.file_path(**file_params, file_format="nc")
                monthly_path = self.file_path(
                    **file_params, suffix="monthly", file_format="nc"
                )

                if len(content) == 1 and period_directory_path in content:
                    statuses[model] = Status.UNAVAILABLE
                elif monthly_path in content:
                    statuses[model] = Status.DOWNSCALED
                elif base_path in content:
                    statuses[model] = Status.DOWNLOADED
        return statuses

    def get_values(
        self,
        location: Point | dict | tuple[int | float, int | float],
        scenario: _Sequence[_Scenario] | _Scenario | None = None,
        model: _Sequence[_Model] | _Model | None = None,
        return_type: str = "df",
    ) -> IndicatorDataFrame | list:
        args = [self.id]
        if scenario is not None:
            if isinstance(scenario, _Scenario):
                scenario_id = scenario.id
                scenario_filter = " AND scenario_id = %s "
            elif isinstance(scenario, list) or isinstance(scenario, tuple):
                scenario_id = tuple(s.id for s in scenario)
                scenario_filter = " AND scenario_id IN %s "
            args.append(scenario_id)
        else:
            scenario_filter = ""

        if model is not None:
            if isinstance(model, _Model):
                model_id = model.id
                model_filter = " AND model_id = %s "
            elif isinstance(model, list) or isinstance(model, tuple):
                model_id = tuple(m.id for m in model)
                model_filter = " AND model_id IN %s "
            args.append(model_id)
        else:
            model_filter = ""

        location = Point.parse(location)

        with DBHandler() as db:
            # Find the closest point
            points = [
                (Point(lat=lat, lon=lon), model_id)
                for (lat, lon, model_id) in db.fetch_all(
                    "SELECT DISTINCT ST_Y(geom), ST_X(geom), model_id FROM indicator.monthly_values WHERE indicator_id = %s",
                    (self.id,),
                )
            ]

            model_points = {}

            for point, model in points:
                if model not in model_points.keys():
                    model_points[model] = [point]
                else:
                    model_points[model].append(point)

            models_closest_point = {
                model: location.find_closest(pts) for model, pts in model_points.items()
            }

            points_filters = " OR ".join(
                [
                    " ST_DWithin(geom, ST_SetSRID(ST_MakePoint(%s, %s), 4326), 0.01) "
                    for model in models_closest_point.keys()
                ]
            )

            for pt in models_closest_point.values():
                args.append(pt.lon)
                args.append(pt.lat)

            query = (
                "SELECT val, EXTRACT(MONTH FROM timestp)::integer AS mo, EXTRACT(YEAR FROM timestp)::integer AS yr, scenario_id, model_id "
                " FROM indicator.monthly_values WHERE indicator_id = %s "
                + scenario_filter
                + model_filter
                + " AND "
                + f" ({points_filters}) "
                # "AND geom = ST_SetSRID(ST_MakePoint(%s, %s), 4326)",
                + " ORDER BY scenario_id, model_id, yr, mo;"
            )

            values = db.fetch_all(query, tuple(arg for arg in args))

            if return_type == "raw":
                return values
            else:
                return self.make_df_from_raw(values)

    def make_df_from_raw(self, values):
        dict_values = [
            {
                "value": val[0],
                "month": val[1],
                "year": val[2],
                "scenario": parse_scenario(val[3]),
                "model": parse_model(val[4]),
                "indicator": self,
            }
            for val in values
        ]

        return IndicatorDataFrame(dict_values, unit=self.unit)

    def locale_name(self):
        return _(f"indicators.{self.variable_name}.name")

    def __str__(self):
        return self.variable_name

    def __repr__(self):
        return f"Indicator({self.variable_name})"

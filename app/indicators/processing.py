from collections.abc import Generator
from datetime import datetime, timedelta
from math import ceil, floor, inf

from netCDF4 import Dataset as _NCDataset

from app.time.utils import parse_date_in_days_since


class NetCDFFile:
    def __init__(self, path: str):
        self.path = path
        self.nc = _NCDataset(self.path)
        self.variable_name = self.get_variable_name()
        self.extract_coordinates_and_variable()

    def get_variable_name(self):
        """Returns the name of the NetCDF variable of this file."""
        return self.nc.variable_id

    def extract_coordinates_and_variable(self):
        """Extracts space/time coordinates and variable arrays from a NetCDF file.

        Returns:
            a tuple with four arrays : latitutdes, longitudes, time and the variable
        """
        var_name = self.get_variable_name()

        self.lat = self.nc.variables["lat"][:]
        self.lon = self.nc.variables["lon"][:]
        self.time = self.nc.variables["time"][:]
        self.variable = self.nc.variables[var_name][:]

        return self.lat, self.lon, self.time, self.variable

    def yield_batch(
        self, batch_size: int = 10000, limit=inf
    ) -> Generator[list[tuple[float, float, datetime, float]]]:
        """Yield `batch_size` variables to put in the database as 4-uples (lat, long, time, variable)

        Args:
            batch_size: the size of batches to yield (number of tuples), defaults to 10000
            limit: optional limit of

        """
        initial_date = parse_date_in_days_since(self.nc.parent_time_units)

        n_iter = 0
        accumulator = []

        for t, days_since in enumerate(self.time):
            if n_iter >= limit:
                break

            timestamp = initial_date + timedelta(days=days_since)

            for lo, longitude in enumerate(self.lon):
                for la, latitude in enumerate(self.lat):
                    v = self.variable[t, la, lo]
                    accumulator.append((longitude, latitude, timestamp, v))
                    n_iter += 1
                    if n_iter % batch_size == 0 and n_iter != 0:
                        yield accumulator
                        accumulator = []
        if len(accumulator) != 0:
            yield accumulator

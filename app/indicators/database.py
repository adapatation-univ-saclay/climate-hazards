from app.database.handler import DBHandler
from app.datasets.database import SCHEMA as DATASET_SCHEMA
from app.datasets.database import TABLE as DATASET_TABLE
from app.hazards.database import SCHEMA as HAZARD_SCHEMA
from app.hazards.database import TABLE as HAZARD_TABLE
from app.indicators.indicators import all_indicators

SCHEMA = "indicator"
TABLE = "indicators"


def insert_all():
    with DBHandler() as db:
        dataset_ids = {
            name: idx
            for (idx, name) in db.fetch_all(
                f"SELECT id, name FROM {DATASET_SCHEMA}.{DATASET_TABLE};"
            )
        }

        hazard_ids = {
            name: idx
            for (idx, name) in db.fetch_all(
                f"SELECT id, name FROM {HAZARD_SCHEMA}.{HAZARD_TABLE};"
            )
        }

        indicators = [
            (
                indicator.name,
                hazard_ids[indicator.hazard.abbreviation],
                dataset_ids[indicator.dataset.name],
                indicator.variable_name,
                indicator.temporal_resolution.copernicus_resolution,
                str(indicator.unit),
            )
            for indicator in all_indicators
        ]

        db.upsert_silent(
            schema=SCHEMA,
            table=TABLE,
            columns=[
                "name",
                "hazard_id",
                "dataset_id",
                "variable_name",
                "temporal_resolution",
                "unit",
            ],
            values=indicators,
            conflict_on="name",
        )


def set_ids():
    with DBHandler() as db:
        ids = {
            name: idx
            for (idx, name) in db.fetch_all(f"SELECT id, name FROM {SCHEMA}.{TABLE};")
        }

        for indicator in all_indicators:
            name = indicator.name
            idx = ids[name]
            indicator.id = idx


def init():
    insert_all()
    set_ids()

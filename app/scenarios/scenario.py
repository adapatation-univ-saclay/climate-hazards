from enum import StrEnum as _StrEnum


class ScenarioType(_StrEnum):
    HISTORICAL = "HISTORICAL"
    RCP = "RCP"
    SSP = "SSP"


class Scenario:
    def __init__(
        self,
        name: str,
        scenario_type: ScenarioType,
        radiative_forcing: float,
        color: str | None = None,
    ):
        """
        Args:
            name: Full name of the scenario
            scenario_type: type (RCP/SSP) of the scenario
            radiative_forcing: value of the radiative forcing associated with the scenario
            color: an optional color for the scenario (for plots)
        """
        self.name = name
        self.scenario_type = scenario_type
        self.radiative_forcing = radiative_forcing
        self.color = color

    # Properties
    @property
    def name(self) -> str:
        """The full name of the scenario"""
        return self._name

    @name.setter
    def name(self, value: str):
        self._name = value

    @property
    def scenario_type(self) -> ScenarioType:
        """The scenario type of the indicator"""
        return self._scenario_type

    @scenario_type.setter
    def scenario_type(self, value: ScenarioType):
        self._scenario_type = value

    def to_copernicus_name(self) -> str:
        return self.name.lower().replace(".", "_").replace(" ", "_").replace("-", "_")

    @property
    def id(self) -> int:
        """The scenario id in database"""
        return self._id

    @id.setter
    def id(self, value: int):
        self._id = value

    @property
    def color(self) -> str:
        """The scenario color hexadecimal string"""
        return self._color

    @color.setter
    def color(self, value: str):
        self._color = value

    def __str__(self):
        return self.name

    def __repr__(self):
        return f"Scenario({str(self)})"

    def __ge__(self, other):
        return str(self) >= str(other)

    def __gt__(self, other):
        return str(self) > str(other)

    def __le__(self, other):
        return str(self) <= str(other)

    def __lt__(self, other):
        return str(self) < str(other)

from app.scenarios.scenario import Scenario as _Scenario
from app.scenarios.scenario import ScenarioType as _ScenarioType


class _RCP(_Scenario):
    def __init__(self, name: str, radiative_forcing: float, color: str | None = None):
        _Scenario.__init__(
            self,
            name=name,
            scenario_type=_ScenarioType.RCP,
            radiative_forcing=radiative_forcing,
            color=color,
        )


RCP_2_6 = _RCP(name="RCP 2.6", radiative_forcing=2.6, color="#003466")
RCP_4_5 = _RCP(name="RCP 4.5", radiative_forcing=4.5, color="#709fcc")
RCP_8_5 = _RCP(name="RCP 8.5", radiative_forcing=8.5, color="#980002")

all_rcp_scenarios = [RCP_2_6, RCP_4_5, RCP_8_5]

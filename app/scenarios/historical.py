from app.scenarios.scenario import Scenario as _Scenario
from app.scenarios.scenario import ScenarioType as _ScenarioType


class _Historical(_Scenario):
    def __init__(self, name: str):
        _Scenario.__init__(self, name, _ScenarioType.HISTORICAL, None)


HISTORICAL = _Historical("historical")

all_historical = [HISTORICAL]

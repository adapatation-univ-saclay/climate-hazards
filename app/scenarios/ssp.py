from enum import StrEnum as _StrEnum

from app.scenarios.scenario import Scenario as _Scenario
from app.scenarios.scenario import ScenarioType as _ScenarioType


class _SocioEconomicType(_StrEnum):
    SSP1 = "Sustainability"
    SSP2 = "Middle of the road"
    SSP3 = "Regional rivalry"
    SSP4 = "Inequality"
    SSP5 = "Fossil fueled development"


class _SSP(_Scenario):
    def __init__(
        self,
        name: str,
        radiative_forcing: float,
        socio_economic_type: _SocioEconomicType,
        color: str | None = None,
    ):
        _Scenario.__init__(
            self,
            name=name,
            scenario_type=_ScenarioType.SSP,
            radiative_forcing=radiative_forcing,
            color=color,
        )
        self.socio_economic_type = socio_economic_type


SSP1_2_6 = _SSP(
    "SSP1-2.6",
    radiative_forcing=2.6,
    socio_economic_type=_SocioEconomicType.SSP1,
    color="#003466",
)
SSP2_4_5 = _SSP(
    "SSP2-4.5",
    radiative_forcing=4.5,
    socio_economic_type=_SocioEconomicType.SSP2,
    color="#f69320",
)
SSP5_8_5 = _SSP(
    "SSP5-8.5",
    radiative_forcing=8.5,
    socio_economic_type=_SocioEconomicType.SSP5,
    color="#980002",
)

all_ssp_scenarios = [SSP1_2_6, SSP2_4_5, SSP5_8_5]

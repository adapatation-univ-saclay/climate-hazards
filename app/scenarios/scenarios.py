from app.scenarios import historical, rcp, ssp

all_scenarios = [
    *historical.all_historical,
    *rcp.all_rcp_scenarios,
    *ssp.all_ssp_scenarios,
]

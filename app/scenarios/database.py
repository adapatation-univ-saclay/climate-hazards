from app.database.handler import DBHandler
from app.scenarios.scenarios import all_scenarios

SCHEMA = "scenario"
TABLE = "scenarios"


def get_socio_economic_type(scenario):
    if hasattr(scenario, "socio_economic_type"):
        return str(scenario.socio_economic_type)
    else:
        return None


def insert_all():
    with DBHandler() as db:
        scenarios = [
            (
                scenario.name,
                (
                    str(scenario.scenario_type)
                    if hasattr(scenario, "scenario_type")
                    else None
                ),
                (
                    scenario.radiative_forcing
                    if hasattr(scenario, "radiative_forcing")
                    else None
                ),
                get_socio_economic_type(scenario),
            )
            for scenario in all_scenarios
        ]
        db.upsert_silent(
            schema=SCHEMA,
            table=TABLE,
            columns=[
                "name",
                "scenario_type",
                "radiative_forcing",
                "socio_economic_type",
            ],
            values=scenarios,
            conflict_on="name",
        )


def set_ids():
    with DBHandler() as db:
        ids = {
            name: idx
            for (idx, name) in db.fetch_all(f"SELECT id, name FROM {SCHEMA}.{TABLE};")
        }

        for scenario in all_scenarios:
            name = scenario.name
            idx = ids[name]
            scenario.id = idx


def init():
    insert_all()
    set_ids()

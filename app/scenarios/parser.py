from app.scenarios.historical import HISTORICAL as _HISTORICAL
from app.scenarios.rcp import RCP_2_6 as _RCP_2_6
from app.scenarios.rcp import RCP_4_5 as _RCP_4_5
from app.scenarios.rcp import RCP_8_5 as _RCP_8_5
from app.scenarios.scenario import Scenario as _Scenario
from app.scenarios.scenarios import all_scenarios
from app.scenarios.ssp import SSP1_2_6 as _SSP1_2_6
from app.scenarios.ssp import SSP2_4_5 as _SSP2_4_5
from app.scenarios.ssp import SSP5_8_5 as _SSP5_8_5

__scenarios_by_id = {}


def parse(value: str | int | _Scenario) -> _Scenario:
    global __scenarios_by_id
    if len(__scenarios_by_id) == 0:
        __scenarios_by_id = {scenario.id: scenario for scenario in all_scenarios}

    if isinstance(value, str):
        match value.lower().replace(" ", "").replace("-", "").replace(".", ""):
            case "historical":
                return _HISTORICAL
            case "rcp26":
                return _RCP_2_6
            case "rcp45":
                return _RCP_4_5
            case "rcp85":
                return _RCP_8_5
            case "ssp126":
                return _SSP1_2_6
            case "ssp245":
                return _SSP2_4_5
            case "ssp585":
                return _SSP5_8_5
    elif isinstance(value, int):
        if value in __scenarios_by_id.keys():
            return __scenarios_by_id[value]
        else:
            raise ValueError(f"Unknown scenario id {repr(value)}")
    elif isinstance(value, _Scenario):
        return value
    else:
        raise TypeError(f"Cannot parse a scenario from a value of type {type(value)}")

from datetime import datetime, timedelta


def parse_date_in_days_since(days_since: str):
    """Parses the start date from a 'days since' string.

    Args:
        days_since: the 'days since' string to parse. e.g.: "days since 1850-01-01"

    Returns:
        a datetime object representing the start date
    """
    ds = "days since "
    start_timestamp = days_since[len(ds) :]
    return datetime.strptime(start_timestamp, "%Y-%m-%d %H:%M:%S")

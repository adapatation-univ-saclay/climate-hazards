from __future__ import annotations

from dataclasses import dataclass
from typing import Type

from app.time.period import (
    Period,
    Month,
    Year,
    FiveYears,
    Decade,
    TwentyYears,
    ThirtyYears,
)


@dataclass
class Resolution:
    number: int
    period: Type[Period]

    @staticmethod
    def parse(val: str) -> Resolution:
        num, per = val.split(" ")
        num = int(num)
        per = Period.parse(per)
        return Resolution(num, per)

from __future__ import annotations

import calendar as _calendar
from abc import abstractmethod
from collections.abc import Callable, Generator
from dataclasses import dataclass
from datetime import datetime, timedelta
from enum import Enum
from typing import Type

two_digits_format = lambda x: str(x).zfill(2)

ALL_DAYS = [two_digits_format(d) for d in range(1, 31 + 1)]
ALL_MONTHS = [two_digits_format(m) for m in range(1, 12 + 1)]


def _add_1_month(dt: datetime) -> datetime:
    """Add one month to a datetime.

    Args:
        dt: the datetime to which one month should be added.
    Returns:
        a new datetime with one month added.
    """
    y = (dt.month + 1) // 13
    m = (dt.month + 1) % 12
    m = 12 if m == 0 else m
    return datetime(dt.year + y, m, dt.day, dt.hour, dt.minute, dt.second)


def _add_1_day(dt: datetime) -> datetime:
    """Add one day to a datetime.

    Args:
        dt: the datetime to which one day should be added.
    Returns:
        a new datetime with one day added.
    """
    try:
        return datetime(dt.year, dt.month, dt.day + 1, dt.hour, dt.minute, dt.second)
    except:
        return _add_1_month(
            datetime(dt.year, dt.month, 1, dt.hour, dt.minute, dt.second)
        )


def timerange(
    start: datetime, end: datetime, step: Callable[[datetime], datetime]
) -> Generator[datetime, None, None]:
    """Generate a series of datetime from a start, and end and a step function.

    Args:
        start: the beginning datetime
        end: the end datetime
        step: the step function that increments datetime

    Yeilds:
        datetimes incremented by the timestep
    """
    val = start
    while val < end:
        yield val
        val = step(val)


@dataclass
class Period:
    start: datetime
    end: datetime
    copernicus_resolution: str | None

    def convert_period(self, resolution: Period | "Resolution") -> Generator[Period]:
        from app.time.resolution import Resolution  # to prevent circular import

        match resolution:
            case Period():
                period = resolution
                step = period.step
                from_start_time = period.from_start_time
            case Resolution():

                def step(dt: datetime) -> datetime:
                    for i in range(resolution.number):
                        dt = resolution.period.step(dt)
                    return dt

                from_start_time = resolution.period.from_start_time

            case _:
                raise TypeError("Invalid resolution.")
        for dt in timerange(start=self.start, end=self.end, step=step):
            yield from_start_time(dt)

    @staticmethod
    def parse(val: str) -> Type[Period]:
        match val:
            case "days" | "day" | "d":
                return Day
            case "months" | "month" | "m":
                return Month
            case "years" | "year" | "y":
                return Year
            case "five" | "fy":
                return FiveYears
            case "decade" | "dec":
                return Decade
            case "twenty" | "tw":
                return TwentyYears
            case "thirty" | "th":
                return ThirtyYears

    @abstractmethod
    def to_compact_dict_resolution(self, res: Type[Period]):
        pass

    @abstractmethod
    def to_dict(self) -> dict[str, int | str]:
        # TODO: refine with resolution
        pass

    def generic_args(self) -> dict[str, int]:
        args = {}
        if hasattr(self, "day"):
            args["day"] = args.day
        if hasattr(self, "month"):
            args["month"] = args.month
        if hasattr(self, "year"):
            args["year"] = args.year
            args["first_year"] = args.year
        return args

    @abstractmethod
    def describe(self) -> str: ...


class Day(Period):
    copernicus_resolution = "daily"

    @staticmethod
    def step(dt: datetime) -> datetime:
        return _add_1_day(dt)

    @staticmethod
    def from_start_time(dt: datetime) -> Month:
        return Day(year=dt.year, month=dt.month, day=dt.day)

    @staticmethod
    def _compute_end(start: datetime) -> datetime:
        return Day.step(start) - timedelta(days=0, seconds=1)

    def __init__(self, year: int, month: int, day: int, **kwargs):
        self.start = datetime(year, month, day, 0, 0, 0)
        self.end = Day._compute_end(self.start)
        self._year = year
        self._month = month
        self._day = day

    @property
    def year(self) -> int:
        return self._year

    @year.setter
    def year(self, year: int):
        self._year = year
        self.start = datetime(year, self.month, self.day, 0, 0, 0)
        self.end = Day._compute_end(self.start)

    @property
    def month(self) -> int:
        return self._month

    @month.setter
    def month(self, month: int):
        self._month = month
        self.start = datetime(self.year, month, self.day, 0, 0, 0)
        self.end = Day._compute_end(self.start)

    @property
    def day(self) -> int:
        return self._day

    @day.setter
    def day(self, day: int):
        self._day = day
        self.start = datetime(self.year, self.month, day, 0, 0, 0)
        self.end = Day._compute_end(self.start)

    def to_dict(self) -> dict[str, str | int]:
        return {
            "day": two_digits_format(self.day),
            "month": two_digits_format(self.month),
            "year": str(self.year),
        }

    def to_compact_dict_resolution(self, res: Type[Period]):
        new_res = res(**self.generic_args())
        return new_res.to_dict()

    def describe(self) -> str:
        return f"{self.year}_{self.month}_{self.day}"


class Month(Period):
    copernicus_resolution = "monthly"

    @staticmethod
    def step(dt: datetime) -> datetime:
        return _add_1_month(dt)

    @staticmethod
    def from_start_time(dt: datetime) -> Month:
        return Month(year=dt.year, month=dt.month)

    @staticmethod
    def _compute_end(start: datetime) -> datetime:
        return Month.step(start) - timedelta(days=0, seconds=1)

    def __init__(self, year: int, month: int, **kwargs):
        self.start = datetime(year, month, 1, 0, 0, 0)
        self.end = Month._compute_end(self.start)
        self._year = year
        self._month = month

    @property
    def year(self) -> int:
        return self._year

    @year.setter
    def year(self, year: int):
        self._year = year
        self.start = datetime(year, self.month, 1, 0, 0, 0)
        self.end = Month._compute_end(self.start)

    @property
    def month(self) -> int:
        return self._month

    @month.setter
    def month(self, month: int):
        self._month = month
        self.start = datetime(self.year, month, 1, 0, 0, 0)
        self.end = Month._compute_end(self.start)

    def to_dict(self) -> dict[str, str | int]:
        return {
            "month": two_digits_format(self.month),
            "year": str(self.year),
        }

    def to_compact_dict_resolution(self, res: Type[Period]):
        args = self.generic_args()
        if res is Day:
            month_dict = self.dict()
            return {
                "day": list(
                    map(
                        two_digits_format,
                        _calendar.itermonthdays(self.year, self.month),
                    )
                ),
                **month_dict,
            }
        # résolution supérieure
        new_res = res(**self.generic_args())
        return new_res.to_dict()

    def describe(self) -> str:
        return f"{self.year}_{self.month}"


class Year(Period):
    copernicus_resolution = "yearly"

    @staticmethod
    def from_start_time(dt: datetime) -> Year:
        return Year(year=dt.year)

    @staticmethod
    def _compute_end(start: datetime) -> datetime:
        return Year.step(start) - timedelta(days=0, seconds=1)

    def __init__(self, year: int, **kwargs):
        self.year = year

    @staticmethod
    def step(dt: datetime) -> datetime:
        return datetime(dt.year + 1, dt.month, dt.day, dt.hour, dt.minute, dt.second)

    @property
    def year(self) -> int:
        return self._year

    @year.setter
    def year(self, year: int):
        self._year = year
        self.start = datetime(year, 1, 1, 0, 0, 0)
        self.end = Year._compute_end(self.start)

    def to_dict(self) -> dict[str, str | int]:
        return {
            "year": str(self.year),
        }

    def to_compact_dict_resolution(self, res: Type[Period]):
        args = self.generic_args()
        year_dict = self.dict()
        if res is Day:
            return {
                "day": ALL_DAYS,
                "month": ALL_MONTHS,
                **year_dict,
            }
        if res is Month:
            return {
                "month": ALL_MONTHS,
                **year_dict,
            }
        # résolution supérieure
        new_res = res(**self.generic_args())
        return new_res.to_dict()

    def describe(self) -> str:
        return f"{self.year}"


class YearRange(Period):
    copernicus_resolution = None

    def __init__(self, first_year: int, duration: int, **kwargs):
        self.duration = duration
        self.first_year = first_year

    def step(self, dt: datetime) -> datetime:
        return datetime(
            dt.year + self.duration, dt.month, dt.day, dt.hour, dt.minute, dt.second
        )

    def _compute_end(self) -> datetime:
        return self.step(self.start) - timedelta(days=0, seconds=1)

    @property
    def first_year(self) -> int:
        return self._first_year

    @first_year.setter
    def first_year(self, first_year: int):
        self._first_year = first_year
        self.start = datetime(first_year, 1, 1)
        self.end = self._compute_end()

    @property
    def duration(self) -> int:
        return self._duration

    @duration.setter
    def duration(self, duration: int):
        self._duration = duration

    def serialize(self, **kwargs) -> dict[str, int]:
        return {"first_year": self.first_year, "duration": self.duration}

    @staticmethod
    def parse(value) -> YearRange:
        if type(value) == dict:
            if (first_year := value.get("first_year")) and (
                duration := value.get("duration")
            ):
                return YearRange(first_year=first_year, duration=duration)
            raise ValueError(
                f"Could not parse a YearRange from given value {repr(value)}."
            )

    def to_dict(self) -> dict[str, str]:
        return {
            "year": [
                str(y) for y in range(self.first_year, self.first_year + self.duration)
            ]
        }

    def to_compact_dict_resolution(self, res: Type[Period]) -> dict[str, str]:
        years_dict = self.to_dict()
        if res is Day:
            return {"day": ALL_DAYS, "month": ALL_MONTHS, **years_dict}
        if res is Month:
            return {"month": ALL_MONTHS, **years_dict}
        return years_dict

    def describe(self) -> str:
        return f"{self.first_year}-{self.first_year + self.duration}"


def multi_year_period_factory(step_number: int):
    class MultiYearPeriodClass(Period):
        @staticmethod
        def from_start_time(dt: datetime) -> MultiYearPeriodClass:
            return MultiYearPeriodClass(first_year=dt.year)

        def __init__(self, first_year: int, **kwargs):
            self.first_year = first_year

        @staticmethod
        def step(dt: datetime) -> datetime:
            return datetime(
                dt.year + step_number, dt.month, dt.day, dt.hour, dt.minute, dt.second
            )

        @staticmethod
        def _compute_end(start: datetime) -> datetime:
            return MultiYearPeriodClass.step(start) - timedelta(days=0, seconds=1)

        @property
        def first_year(self) -> int:
            return self._first_year

        @first_year.setter
        def first_year(self, first_year: int):
            self._first_year = first_year
            self.start = datetime(first_year, 1, 1)
            self.end = MultiYearPeriodClass._compute_end(self.start)

        def to_dict(self) -> dict[str, str]:
            return {
                "year": [
                    str(y)
                    for y in range(self.first_year, self.first_year + step_number)
                ],
            }

        def to_compact_dict_resolution(self, res: Type[Period]) -> dict[str, str]:
            years_dict = self.to_dict()
            if res is Day:
                return {
                    "day": ALL_DAYS,
                    "month": ALL_MONTHS,
                    **years_dict,
                }
            if res is Month:
                return {
                    "month": ALL_MONTHS,
                    **years_dict,
                }
            return years_dict

    return MultiYearPeriodClass


FiveYears = multi_year_period_factory(5)
Decade = multi_year_period_factory(10)
TwentyYears = multi_year_period_factory(20)
ThirtyYears = multi_year_period_factory(30)
FiftyYears = multi_year_period_factory(50)

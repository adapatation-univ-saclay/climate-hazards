import os as _os
from pathlib import Path as _Path

import i18n as _i18n

_i18n.set("file_format", "json")

DEFAULT_LANGUAGE = "fr"
TRANSLATIONS_DIR = _os.path.realpath(
    _os.path.join(
        _os.path.dirname(_os.path.realpath(__file__)), "..", "res", "translations"
    )
)


all_translation_dirs = list(_Path(TRANSLATIONS_DIR).rglob("**"))

_i18n.load_path.append(TRANSLATIONS_DIR)


def _(key: str, **kwargs):
    return _i18n.t(key, **kwargs)


def set_locale(locale):
    _i18n.set("locale", locale)


set_locale(DEFAULT_LANGUAGE)

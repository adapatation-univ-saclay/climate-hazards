import app.categories as categories
import app.hazards as hazards
import app.indicators as indicators
import app.models as models
import app.regions as regions
import app.scenarios as scenarios
import app.time as time
import app.units as units
from app.startup import init

init()

from collections.abc import Iterable
from typing import Self

import psycopg2
from psycopg2.extras import Json as Jsonify
from psycopg2.extras import execute_values

import config as _config


def exec_with_optional_args(func, query, optional_args):
    if optional_args:
        return func(query, optional_args)
    else:
        return func(query)


def Json(dic):
    """Adapt dictionary to postgresql jsonb object"""
    return Jsonify(dic)


class DBHandler:
    def __init__(
        self, config: _config.DatabaseConfig = _config.DATABASE, autocommit: bool = True
    ):
        self.config = config
        self.conn = None
        self.cur = None
        self.autocommit = True

    def make_connection_args(self) -> dict:
        """Create database connection kwargs from DBHandler's database config."""
        return {
            "dbname": self.config.database,
            "user": self.config.username,
            "password": self.config.password,
            "host": self.config.host,
            "port": 5432,
        }

    def connect(self) -> Self:
        conn_args = self.make_connection_args()
        self.conn = psycopg2.connect(**conn_args)
        self.cur = self.conn.cursor()
        return self

    def execute(self, query: str, optional_args=None):
        """Execute an SQL query.

        Args:
            query: the query to execute
            optional_args: optional arguments that would be added to psycopg's `execute` function
        """
        if not self.conn:
            self.connect()

        res = exec_with_optional_args(self.cur.execute, query, optional_args)

        return res

    def fetch(self, query: str, size: int = 1, optional_args=None):
        """Execute an SQL query and fetch associated results.

        Args:
            query: the query to execute
            size: the number of results to fetch, defaults to 1.
            optional_args: optional arguments that would be added to psycopg's `execute` function
        """
        self.execute(query, optional_args)
        if size == 1:
            return self.cur.fetchone()
        else:
            return self.cur.fetchmany(size)

    def fetch_all(self, query: str, optional_args=None):
        """Execute an SQL query and fetch all associated results.

        Args:
            query: the query to execute.
            optional_args: optional arguments that would be added to psycopg's `execute` function.
        """
        self.execute(query, optional_args)
        return self.cur.fetchall()

    def batch(self, query: str, batch_size: int = 10, optional_args=None):
        """Execute an SQL query and fetch batches of results.

        Args:
            query: the query to execute.
            batch_size: the size of batches that will be retrieved from the database.
            optional_args: optional arguments that would be added to psycopg's `execute` function.
        """
        self.execute(query, optional_args)
        for rows in self.cur.fetchmany(size=batch_size):
            yield rows

    @staticmethod
    def full_table_name(schema: str | None, table: str):
        schema_str = "" if schema is None else f"{schema}."
        return schema_str + table

    def insert(
        self,
        schema: str | None,
        table: str,
        columns: Iterable[str],
        values: list,
    ):
        """Insert one or multiple values in a given database table."""
        full_name = self.full_table_name(schema, table)

        quoted_columns = [f'"{column}"' for column in columns]
        columns_str = ",".join(quoted_columns)

        res = execute_values(
            self.cur,
            f"INSERT INTO {full_name}({columns_str}) VALUES %s",
            values,
        )

        return res

    def upsert_silent(
        self,
        schema: str | None,
        table: str,
        columns: list[str] | tuple[str],
        values: list,
        conflict_on: str,
    ):
        """Insert a row or do nothing on conflict (meaning that the row won't be
        inserted if another record matches this columns' value).

        Args:
            schema: the (optional) name of the schema that contains the table to insert into
            table: the name of the table to insert values into
            columns: the list of column names to insert
            values: a list of rows to insert, each row being a sequence of the
                rows' values for the given `columns` in the same order
            conflict_on: the name of the row to check for conflict on (requires a unique index)
        """
        full_name = self.full_table_name(schema, table)

        quoted_columns = [f'"{column}"' for column in columns]
        quoted_conflict_on = f'"{conflict_on}"'

        columns_str = ",".join(quoted_columns)

        res = execute_values(
            self.cur,
            f"""INSERT INTO {full_name}({columns_str}) VALUES %s
            ON CONFLICT({quoted_conflict_on}) DO NOTHING;""",
            values,
        )

        return res

    def close(self):
        if self.conn:
            if self.autocommit:
                self.conn.commit()
            self.cur.close()
            self.conn.close()
        self.conn = None
        self.cur = None

    def __del__(self):
        self.close()

    # Use as context manager

    def __enter__(self):
        self.connect()
        return self

    def __exit__(self, type, value, traceback):
        self.close()

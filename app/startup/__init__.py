from app.startup.database import init_database


def init():
    init_database()

from app.categories.database import init as init_categories
from app.datasets.database import init as init_datasets
from app.hazards.database import init as init_hazards
from app.indicators.database import init as init_indicators
from app.models.database import init as init_institutes_and_models
from app.scenarios.database import init as init_scenarios


def init_database():
    init_categories()
    init_hazards()
    init_datasets()
    init_scenarios()
    init_institutes_and_models()
    init_indicators()

from pathlib import Path

from fastapi import Request

from app.locale import _


class Hazard:
    def __init__(
        self,
        category: str,
        abbreviation: str,
    ):
        self.category = category
        self.abbreviation = abbreviation

    def name(self):
        """Return the full, localized name of the hazard."""
        return _(f"hazards.{self.category}.{self.abbreviation}.name")

    def description(self):
        """Return the localized description of the hazard."""
        return _(f"hazards.{self.category}.{self.abbreviation}.description")

    @property
    def id(self) -> int:
        """The hazard id in database"""
        return self._id

    @id.setter
    def id(self, id: int):
        self._id = id

    def serialize(self, depth: int = 1, base_url: str | None = None):
        """Serialize the hazard into a dictionary.

        Args:
            depth: the depth of serialization for nested objects, defaults to 1
            base_url: the base api URL, defaults to None. If not None, will provide
                the objects' api URL

        Returns:
            the hazard, serialized as a dictionary.
        """
        s = {
            "name": self.name(),
            "description": self.description(),
            "category": self.category,
            "abbreviation": self.abbreviation,
        }
        if base_url:
            s["url"] = f"{base_url}hazards/{self.abbreviation}"
        return s

from app.hazards.hazard import Hazard as _Hazard

_CAT = "ocean"

mean_ocean_temperature = _Hazard(category=_CAT, abbreviation="mean_ocean_temperature")
marine_heatwave = _Hazard(category=_CAT, abbreviation="marine_heatwave")
ocean_acidity = _Hazard(category=_CAT, abbreviation="ocean_acidity")
ocean_salinity = _Hazard(category=_CAT, abbreviation="ocean_salinity")
dissolved_oxygen = _Hazard(category=_CAT, abbreviation="dissolved_oxygen")

_all_hazards = [
    mean_ocean_temperature,
    marine_heatwave,
    ocean_acidity,
    ocean_salinity,
    dissolved_oxygen,
]

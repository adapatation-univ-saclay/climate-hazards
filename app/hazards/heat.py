from app.hazards.hazard import Hazard as _Hazard

_CAT = "heat_and_cold"

mean_air_temperature = _Hazard(category=_CAT, abbreviation="mean_air_temperature")
extreme_heat = _Hazard(category=_CAT, abbreviation="extreme_heat")
cold_spell = _Hazard(category=_CAT, abbreviation="cold_spell")
frost = _Hazard(category=_CAT, abbreviation="frost")

_all_hazards = [mean_air_temperature, extreme_heat, cold_spell, frost]

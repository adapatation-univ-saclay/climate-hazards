from app.categories.categories import all_categories
from app.categories.database import SCHEMA as CATEGORY_SCHEMA
from app.categories.database import TABLE as CATEGORY_TABLE
from app.database.handler import DBHandler

SCHEMA = "hazard"
TABLE = "hazards"


def insert_all():
    with DBHandler() as db:
        category_ids = {
            name: idx
            for (idx, name) in db.fetch_all(
                f"SELECT id, name FROM {CATEGORY_SCHEMA}.{CATEGORY_TABLE};"
            )
        }

        values = [
            (hazard.abbreviation, category_ids[abbr])
            for (abbr, category) in all_categories.items()
            for hazard in category.hazards
        ]

        db.upsert_silent(
            schema=SCHEMA,
            table=TABLE,
            columns=["name", "category_id"],
            values=values,
            conflict_on="name",
        )


def set_ids():
    with DBHandler() as db:
        ids = {
            name: idx
            for (idx, name) in db.fetch_all(f"SELECT id, name FROM {SCHEMA}.{TABLE};")
        }

        all_hazards = [
            hazard
            for category in all_categories.values()
            for hazard in category.hazards
        ]

        for hazard in all_hazards:
            name = hazard.abbreviation
            idx = ids[name]
            hazard.id = idx


def init():
    insert_all()
    set_ids()

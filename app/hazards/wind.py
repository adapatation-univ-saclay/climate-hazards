from app.hazards.hazard import Hazard as _Hazard

_CAT = "wind"

mean_wind_speed = _Hazard(category=_CAT, abbreviation="mean_wind_speed")
severe_wind_storm = _Hazard(category=_CAT, abbreviation="severe_wind_storm")
tropical_cyclone = _Hazard(category=_CAT, abbreviation="tropical_cyclone")
sand_dust_storm = _Hazard(category=_CAT, abbreviation="sand_dust_storm")

_all_hazards = [
    mean_wind_speed,
    severe_wind_storm,
    tropical_cyclone,
    sand_dust_storm,
]

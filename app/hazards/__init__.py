import app.hazards.heat as _heat
import app.hazards.drought as _drought
import app.hazards.wind as _wind
import app.hazards.snow as _snow
import app.hazards.coastal as _coastal
import app.hazards.ocean as _ocean
import app.hazards.other as _other

all_hazards = {}
for cat in [_heat, _drought, _wind, _snow, _coastal, _ocean, _other]:
    for haz in cat._all_hazards:
        all_hazards[haz.abbreviation] = haz

from app.hazards.hazard import Hazard as _Hazard

_CAT = "snow_and_ice"

snow_glacier_sheet = _Hazard(category=_CAT, abbreviation="snow_glacier_sheet")
permafrost = _Hazard(category=_CAT, abbreviation="permafrost")
lake_river_sea = _Hazard(category=_CAT, abbreviation="lake_river_sea")
heavy_snowfall = _Hazard(category=_CAT, abbreviation="heavy_snowfall")
hail = _Hazard(category=_CAT, abbreviation="hail")
snow_avalanche = _Hazard(category=_CAT, abbreviation="snow_avalanche")

_all_hazards = [
    snow_glacier_sheet,
    permafrost,
    lake_river_sea,
    heavy_snowfall,
    hail,
    snow_avalanche,
]

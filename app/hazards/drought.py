from app.hazards.hazard import Hazard as _Hazard

_CAT = "wet_and_dry"

mean_precipitation = _Hazard(category=_CAT, abbreviation="mean_precipitation")
river_flood = _Hazard(category=_CAT, abbreviation="river_flood")
heavy_precipitation = _Hazard(category=_CAT, abbreviation="heavy_precipitation")
landslide = _Hazard(category=_CAT, abbreviation="landslide")
aridity = _Hazard(category=_CAT, abbreviation="aridity")
hydrological_drought = _Hazard(category=_CAT, abbreviation="hydrological_drought")
agricultural_drought = _Hazard(category=_CAT, abbreviation="agricultural_drought")
fire_weather = _Hazard(category=_CAT, abbreviation="fire_weather")

_all_hazards = [
    mean_precipitation,
    river_flood,
    heavy_precipitation,
    landslide,
    aridity,
    hydrological_drought,
    agricultural_drought,
    fire_weather,
]

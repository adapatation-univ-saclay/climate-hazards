from app.hazards.hazard import Hazard as _Hazard

_CAT = "other"

air_pollution_weather = _Hazard(category=_CAT, abbreviation="air_pollution_weather")
co2_at_surface = _Hazard(category=_CAT, abbreviation="co2_at_surface")
radiation_at_surface = _Hazard(category=_CAT, abbreviation="radiation_at_surface")

_all_hazards = [
    air_pollution_weather,
    co2_at_surface,
    radiation_at_surface,
]

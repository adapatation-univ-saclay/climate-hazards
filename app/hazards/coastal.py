from app.hazards.hazard import Hazard as _Hazard

_CAT = "coastal"

relative_sea_level = _Hazard(category=_CAT, abbreviation="relative_sea_level")
coastal_flood = _Hazard(category=_CAT, abbreviation="coastal_flood")
coastal_erosion = _Hazard(category=_CAT, abbreviation="coastal_erosion")

_all_hazards = [
    relative_sea_level,
    coastal_flood,
    coastal_erosion,
]

from app.datasets.dataset import Dataset
from app.scenarios.historical import HISTORICAL
from app.scenarios.ssp import SSP1_2_6, SSP2_4_5, SSP5_8_5
from app.time.period import YearRange

CMIP6_PROJECTIONS = Dataset(
    name="projections-cmip6",
    available_periods={
        HISTORICAL.name: YearRange(1950, 65),
        SSP1_2_6.name: YearRange(2015, 86),
        SSP2_4_5.name: YearRange(2015, 86),
        SSP5_8_5.name: YearRange(2015, 86),
    },
)

all_datasets = [CMIP6_PROJECTIONS]

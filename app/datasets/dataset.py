from dataclasses import dataclass

from app.time.period import Period


@dataclass
class Dataset:
    name: str
    available_periods: dict[str, Period]
    id: int | None = None

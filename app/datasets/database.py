from app.database.handler import DBHandler, Json
from app.datasets.datasets import all_datasets
from app.serialize import serialize

SCHEMA = "indicator"
TABLE = "datasets"


def insert_all():
    with DBHandler() as db:
        datasets = [
            (ds.name, Json(serialize(ds.available_periods))) for ds in all_datasets
        ]
        db.upsert_silent(
            schema=SCHEMA,
            table=TABLE,
            columns=["name", "available_periods"],
            values=datasets,
            conflict_on="name",
        )


def set_ids():
    with DBHandler() as db:
        ids = {
            name: idx
            for (idx, name) in db.fetch_all(f"SELECT id, name FROM {SCHEMA}.{TABLE};")
        }

        for dataset in all_datasets:
            name = dataset.name
            idx = ids[name]
            dataset.id = idx


def init():
    insert_all()
    set_ids()

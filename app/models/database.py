from app.database.handler import DBHandler
from app.models.institutes import _all_institutes
from app.models.models import all_models

SCHEMA = "model"
INSTITUTES_TABLE = "institutes"
MODELS_TABLE = "models"


def insert_all_institutes():
    institutes = [
        (institute.name, institute.acronym, str(institute.country))
        for institute in _all_institutes
    ]
    with DBHandler() as db:
        db.upsert_silent(
            schema=SCHEMA,
            table=INSTITUTES_TABLE,
            columns=["name", "acronym", "country"],
            values=institutes,
            conflict_on="name",
        )


def insert_all_models():
    with DBHandler() as db:
        institute_ids = {
            name: idx
            for (idx, name) in db.fetch_all(
                f"SELECT id, name FROM {SCHEMA}.{INSTITUTES_TABLE};"
            )
        }

        models = [
            (
                model.name,
                model.flat_name(),
                str(model.experiment),
                institute_ids[model.institute.name],
            )
            for model in all_models
        ]

        db.upsert_silent(
            schema=SCHEMA,
            table=MODELS_TABLE,
            columns=[
                "name",
                "flat_name",
                "experiment",
                "institute_id",
            ],
            values=models,
            conflict_on="name",
        )


def set_institute_ids():
    with DBHandler() as db:
        ids = {
            name: idx
            for (idx, name) in db.fetch_all(
                f"SELECT id, name FROM {SCHEMA}.{INSTITUTES_TABLE};"
            )
        }

        for institute in _all_institutes:
            name = institute.name
            idx = ids[name]
            institute.id = idx


def set_model_ids():
    with DBHandler() as db:
        ids = {
            name: idx
            for (idx, name) in db.fetch_all(
                f"SELECT id, name FROM {SCHEMA}.{MODELS_TABLE};"
            )
        }

        for model in all_models:
            name = model.name
            idx = ids[name]
            model.id = idx


def init():
    insert_all_institutes()
    set_institute_ids()
    insert_all_models()
    set_model_ids()

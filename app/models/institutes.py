from dataclasses import dataclass
from enum import StrEnum
from random import choice


class _Country(StrEnum):
    Australia = "Australia"
    China = "China"
    Canada = "Canada"
    Europe = "Europe"
    France = "France"
    Germany = "Germany"
    India = "India"
    Italy = "Italy"
    Japan = "Japan"
    Norway = "Norway"
    Russia = "Russia"
    South_Korea = "South Korea"
    Switzerland = "Switzerland"
    Taiwan = "Taiwan"
    USA = "USA"
    UK = "UK"


@dataclass
class Institute:
    name: str
    country: _Country
    acronym: str
    id: int | None = None

    def serialize(self, develop: int = 1) -> dict:
        """Serialize the institute into a dictionary.

        Args:
            depth: the depth of serialization for nested objects, defaults to 1

        Returns:
            the institute, serialized as a dictionary.
        """
        return {"name": self.name, "country": self.country, "acronym": self.acronym}

    def add_model(self, model):
        if not hasattr(self, "models"):
            self.models = set()
        self.models.add(model)

    def get_random_model(self) -> "Model":
        """Returns a random model, in order to perform a model comparison that
        is balanced between all institute"""
        return choice(list(self.models))


ACCESS = Institute(
    name="Australian Community Climate Earth System Simulators",
    country=_Country.Australia,
    acronym="ACCESS",
)

AWI = Institute(
    name="Alfred Wegener Institute", country=_Country.Germany, acronym="AWI"
)

BCC = Institute(name="Beijing Climate Centre", country=_Country.China, acronym="BCC")

CAMS = Institute(
    name="Chinese Academy of Meteorological Sciences",
    country=_Country.China,
    acronym="CAMS",
)

CCCma = Institute(
    name="Canadian Centre for Climate Modelling and Analysis",
    country=_Country.Canada,
    acronym="CCCma",
)

CMCC = Institute(
    name="Centro Euro-Mediterraneo sui Cambiamenti Climatici",
    country=_Country.Italy,
    acronym="CMCC",
)

CNRM = Institute(
    name="Centre National de Recherches Météorologiques",
    country=_Country.France,
    acronym="CNRM",
)

E3SM = Institute(
    name="Energy Exascale Earth System Model", country=_Country.USA, acronym="E3SM"
)

EC_Earth = Institute(
    name="European community Earth ", country=_Country.Europe, acronym="EC"
)

FIO = Institute(
    name="First Institute of Oceanography", country=_Country.China, acronym="FIO"
)

GFDL = Institute(
    name="Geophysical Fluid Dynamics Laboratory", country=_Country.USA, acronym="GFDL"
)

IAP = Institute(
    name="Institute of Atmospheric Physics", country=_Country.China, acronym="IAP"
)  # FGOALS

IITM = Institute(
    name="Indian Institute of Tropical Meteorology",
    country=_Country.India,
    acronym="IITM",
)

INM = Institute(
    name="Institute for Numerical Mathematics", country=_Country.Russia, acronym="INM"
)

IPSL = Institute(
    name="Institut Pierre Simon Laplace", country=_Country.France, acronym="IPSL"
)

JAMSTEC = Institute(
    name="Japan Agency for Marine-Earth Science and Technology",
    country=_Country.Japan,
    acronym="JAMSTEC",
)  # MIROC

KACE = Institute(
    name="Korea Meteorological Administration",
    country=_Country.South_Korea,
    acronym="KACE",
)

KIOST = Institute(
    name="Korea Institute of Ocean Science and Technology",
    country=_Country.South_Korea,
    acronym="KIOST",
)

MOHC = Institute(
    name="Met Office Hadley Centre", country=_Country.UK, acronym="MOHC"
)  # Had, UKESM

MPI = Institute(name="Max Planck Institute", country=_Country.Germany, acronym="MPI")

MRI = Institute(
    name="Meteorological Research Institute", country=_Country.Japan, acronym="MRI"
)


NASA = Institute(
    name="National Aeronautics and Space Administration",
    country=_Country.USA,
    acronym="NASA",
)  # GISS


NCAR = Institute(
    name="National Center for Atmospheric Research",
    country=_Country.USA,
    acronym="NCAR",
)  # CESM

NCC = Institute(
    name="Norwegian Climate Centre",
    country=_Country.Norway,
    acronym="NCC",
)  # Nor

NUIST = Institute(
    name="Nanjing University of Information Science and Technology",
    country=_Country.China,
    acronym="NIUST",
)  # NESM

RCEC = Institute(
    name="Research Center for Environmental Changes, Academia Sinica",
    country=_Country.Taiwan,
    acronym="RCEC",
)

SNU = Institute(
    name="Seoul National University",
    country=_Country.South_Korea,
    acronym="SNU",
)  # SAM UNICON

Tsinghua_University = Institute(
    name="Community Integrated Earth System Model",
    country=_Country.China,
    acronym="Tsinghua",
)

U_Arizona = Institute(
    name="University of Arizona", country=_Country.USA, acronym="UArizona"
)  # MCM


_all_institutes = [
    ACCESS,
    AWI,
    BCC,
    CAMS,
    CCCma,
    CMCC,
    CNRM,
    E3SM,
    EC_Earth,
    FIO,
    GFDL,
    IAP,
    IITM,
    INM,
    IPSL,
    JAMSTEC,
    KACE,
    KIOST,
    MOHC,
    MPI,
    MRI,
    NASA,
    NCAR,
    NCC,
    NUIST,
    RCEC,
    SNU,
    Tsinghua_University,
    U_Arizona,
]

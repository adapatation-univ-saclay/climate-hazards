from dataclasses import dataclass

from fastapi import Request

from app.models.experiments import Experiment
from app.models.institutes import Institute


class Model:

    def __init__(
        self,
        name: str,
        experiment: Experiment,
        institute: Institute,
        id: int | None = None,
    ):
        self.name = name
        self.experiment = experiment
        self.institute = institute
        self.id = id

    @property
    def name(self) -> str:
        return self._name

    @name.setter
    def name(self, name: str):
        self._name = name

    @property
    def experiment(self) -> Experiment:
        return self._experiment

    @experiment.setter
    def experiment(self, experiment: Experiment):
        self._experiment = experiment

    @property
    def institute(self) -> Institute:
        return self._institute

    @institute.setter
    def institute(self, institute: Institute):
        self._institute = institute
        institute.add_model(self)

    @property
    def id(self) -> int | None:
        return self._id

    @id.setter
    def id(self, id: int | None):
        self._id = id

    def flat_name(self) -> str:
        """The model's name in snake case, as used in Copernicus Climate Data
        Store request."""
        return self.name.replace("-", "_").replace(" ", "_").replace(".", "_").lower()

    def serialize(self, depth: int = 1, base_url: str | None = None) -> dict:
        """Serialize the model into a dictionary.

        Args:
            depth: the depth of serialization for nested objects, defaults to 1
            base_url: the base api URL, defaults to None. If not None, will provide
                the objects' api URL

        Returns:
            the model, serialized as a dictionary.
        """
        s = {
            "name": self.name,
            "flat_name": self.flat_name(),
            "experiment": self.experiment,
            "institute": (
                self.institute.serialize() if depth else self.institute.acronym
            ),
        }
        if base_url:
            s["url"] = f"{base_url}models/{self.flat_name()}"
        return s

    def to_copernicus_name(self) -> str:
        return self.name.lower().replace(".", "_").replace(" ", "_").replace("-", "_")

    def __str__(self):
        return self.name

    def __repr__(self):
        return f"Model({str(self)})"

    def __ge__(self, other):
        return str(self) >= str(other)

    def __gt__(self, other):
        return str(self) > str(other)

    def __le__(self, other):
        return str(self) <= str(other)

    def __lt__(self, other):
        return str(self) < str(other)

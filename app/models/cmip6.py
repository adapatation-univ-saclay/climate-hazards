import app.models.institutes as _i
from app.models.experiments import Experiment as _Experiment
from app.models.model import Model as _Model

ACCESS_CM2 = _Model(
    name="ACCESS-CM2", experiment=_Experiment.CMIP6, institute=_i.ACCESS
)
ACCESS_ESM1_5 = _Model(
    name="ACCESS-ESM1-5", experiment=_Experiment.CMIP6, institute=_i.ACCESS
)

AWI_CM_1_1_MR = _Model(
    name="AWI-CM-1-1-MR", experiment=_Experiment.CMIP6, institute=_i.AWI
)
AWI_ESM_1_1_LR = _Model(
    name="AWI-ESM-1-1-LR", experiment=_Experiment.CMIP6, institute=_i.AWI
)

BCC_CSM2_MR = _Model(name="BCC-CSM2-MR", experiment=_Experiment.CMIP6, institute=_i.BCC)
BCC_ESM1 = _Model(name="BCC-ESM1", experiment=_Experiment.CMIP6, institute=_i.BCC)

CAMS_CSM1_0 = _Model(
    name="CAMS-CSM1-0", experiment=_Experiment.CMIP6, institute=_i.CAMS
)

CanESM5 = _Model(name="CanESM5", experiment=_Experiment.CMIP6, institute=_i.CCCma)
CanESM5_CanOE = _Model(
    name="CanESM5-CanOE", experiment=_Experiment.CMIP6, institute=_i.CCCma
)

CESM2 = _Model(name="CESM2", experiment=_Experiment.CMIP6, institute=_i.NCAR)
CESM2_FV2 = _Model(name="CESM2-FV2", experiment=_Experiment.CMIP6, institute=_i.NCAR)
CESM2_WACCM = _Model(
    name="CESM2-WACCM", experiment=_Experiment.CMIP6, institute=_i.NCAR
)
CESM2_WACCM_FV2 = _Model(
    name="CESM2-WACCM-FV2", experiment=_Experiment.CMIP6, institute=_i.NCAR
)

CIESM = _Model(
    name="CIESM", experiment=_Experiment.CMIP6, institute=_i.Tsinghua_University
)

CMCC_CM2_HR4 = _Model(
    name="CMCC-CM2-HR4", experiment=_Experiment.CMIP6, institute=_i.CMCC
)
CMCC_CM2_SR5 = _Model(
    name="CMCC-CM2-SR5", experiment=_Experiment.CMIP6, institute=_i.CMCC
)
CMCC_ESM2 = _Model(name="CMCC-ESM2", experiment=_Experiment.CMIP6, institute=_i.CMCC)

CNRM_CM6_1 = _Model(name="CNRM-CM6-1", experiment=_Experiment.CMIP6, institute=_i.CNRM)
CNRM_CM6_1_HR = _Model(
    name="CNRM-CM6-1-HR", experiment=_Experiment.CMIP6, institute=_i.CNRM
)
CNRM_ESM2_1 = _Model(
    name="CNRM-ESM2-1", experiment=_Experiment.CMIP6, institute=_i.CNRM
)

E3SM_1_0 = _Model(name="E3SM-1-0", experiment=_Experiment.CMIP6, institute=_i.E3SM)
E3SM_1_1 = _Model(name="E3SM-1-1", experiment=_Experiment.CMIP6, institute=_i.E3SM)
E3SM_1_1_ECA = _Model(
    name="E3SM-1-1-ECA", experiment=_Experiment.CMIP6, institute=_i.E3SM
)

EC_Earth3 = _Model(
    name="EC-Earth3", experiment=_Experiment.CMIP6, institute=_i.EC_Earth
)
EC_Earth3_AerChem = _Model(
    name="EC-Earth3-AerChem", experiment=_Experiment.CMIP6, institute=_i.EC_Earth
)
EC_Earth3_CC = _Model(
    name="EC-Earth3-CC", experiment=_Experiment.CMIP6, institute=_i.EC_Earth
)
EC_Earth3_Veg = _Model(
    name="EC-Earth3-Veg", experiment=_Experiment.CMIP6, institute=_i.EC_Earth
)
EC_Earth3_VegLR = _Model(
    name="EC-Earth3-VegLR", experiment=_Experiment.CMIP6, institute=_i.EC_Earth
)

FGOALS_f3_L = _Model(name="FGOALS-f3-L", experiment=_Experiment.CMIP6, institute=_i.IAP)
FGOALS_g3 = _Model(name="FGOALS-g3", experiment=_Experiment.CMIP6, institute=_i.IAP)

FIO_ESM_2_0 = _Model(name="FIO-ESM-2-0", experiment=_Experiment.CMIP6, institute=_i.FIO)

GFDL_ESM4 = _Model(name="GFDL-ESM4", experiment=_Experiment.CMIP6, institute=_i.GFDL)

GISS_E2_1_G = _Model(
    name="GISS-E2-1-G", experiment=_Experiment.CMIP6, institute=_i.NASA
)
GISS_E2_1_H = _Model(
    name="GISS-E2-1-H", experiment=_Experiment.CMIP6, institute=_i.NASA
)

HadGEM3_GC31_LL = _Model(
    name="HadGEM3-GC31-LL", experiment=_Experiment.CMIP6, institute=_i.MOHC
)
HadGEM3_GC31_MM = _Model(
    name="HadGEM3-GC31-MM", experiment=_Experiment.CMIP6, institute=_i.MOHC
)

IITM_ESM = _Model(name="IITM-ESM", experiment=_Experiment.CMIP6, institute=_i.IITM)

INM_CM4_8 = _Model(name="INM-CM4-8", experiment=_Experiment.CMIP6, institute=_i.INM)
INM_CM5_0 = _Model(name="INM-CM5-0", experiment=_Experiment.CMIP6, institute=_i.INM)

IPSL_CM5A2_INCA = _Model(
    name="IPSL-CM5A2-INCA", experiment=_Experiment.CMIP6, institute=_i.IPSL
)
IPSL_CM6A_LR = _Model(
    name="IPSL-CM6A-LR", experiment=_Experiment.CMIP6, institute=_i.IPSL
)

KACE_1_0_G = _Model(name="KACE-1-0-G", experiment=_Experiment.CMIP6, institute=_i.KACE)

KIOST_ESM = _Model(name="KIOST-ESM", experiment=_Experiment.CMIP6, institute=_i.KIOST)

MCM_UA_1_0 = _Model(
    name="MCM-UA-1-0", experiment=_Experiment.CMIP6, institute=_i.U_Arizona
)

MIROC6 = _Model(name="MIROC6", experiment=_Experiment.CMIP6, institute=_i.JAMSTEC)
MIROC6_ES2H = _Model(
    name="MIROC6-ES2H", experiment=_Experiment.CMIP6, institute=_i.JAMSTEC
)
MIROC6_ES2L = _Model(
    name="MIROC6-ES2L", experiment=_Experiment.CMIP6, institute=_i.JAMSTEC
)

MPI_ESM_1_2_HAM = _Model(
    name="MPI-ESM-1-2-HAM", experiment=_Experiment.CMIP6, institute=_i.MPI
)  # Switzerland

MPI_ESM1_2_HR = _Model(
    name="MPI-ESM1-2-HR", experiment=_Experiment.CMIP6, institute=_i.MPI
)  # Germany
MPI_ESM1_2_LR = _Model(
    name="MPI-ESM1-2-LR", experiment=_Experiment.CMIP6, institute=_i.MPI
)  # Germany

MRI_ESM2_0 = _Model(name="MRI-ESM1-0", experiment=_Experiment.CMIP6, institute=_i.MRI)
NESM3 = _Model(name="NESM3", experiment=_Experiment.CMIP6, institute=_i.NUIST)

NorCPM1 = _Model(name="NESM3", experiment=_Experiment.CMIP6, institute=_i.NCC)
NorESM2_LM = _Model(name="NorESM2-LM", experiment=_Experiment.CMIP6, institute=_i.NCC)
NorESM2_MM = _Model(name="NorESM2-MM", experiment=_Experiment.CMIP6, institute=_i.NCC)

SAM0_UNICON = _Model(name="SAM0-UNICON", experiment=_Experiment.CMIP6, institute=_i.SNU)

TaiESM1 = _Model(name="TaiESM1", experiment=_Experiment.CMIP6, institute=_i.RCEC)

UKESM1_0_LL = _Model(
    name="UKESM1-0-LL", experiment=_Experiment.CMIP6, institute=_i.MOHC
)

_all_models = [
    ACCESS_CM2,
    ACCESS_ESM1_5,
    AWI_CM_1_1_MR,
    AWI_ESM_1_1_LR,
    BCC_CSM2_MR,
    BCC_ESM1,
    CAMS_CSM1_0,
    CanESM5,
    CanESM5_CanOE,
    CESM2,
    CESM2_FV2,
    CESM2_WACCM,
    CESM2_WACCM_FV2,
    CIESM,
    CMCC_CM2_HR4,
    CMCC_CM2_SR5,
    CMCC_ESM2,
    CNRM_CM6_1,
    CNRM_CM6_1_HR,
    CNRM_ESM2_1,
    E3SM_1_0,
    E3SM_1_1,
    E3SM_1_1_ECA,
    EC_Earth3,
    EC_Earth3_AerChem,
    EC_Earth3_CC,
    EC_Earth3_Veg,
    EC_Earth3_VegLR,
    FGOALS_f3_L,
    FGOALS_g3,
    GFDL_ESM4,
    GISS_E2_1_G,
    GISS_E2_1_H,
    HadGEM3_GC31_LL,
    HadGEM3_GC31_MM,
    IITM_ESM,
    INM_CM4_8,
    INM_CM5_0,
    IPSL_CM5A2_INCA,
    IPSL_CM6A_LR,
    KACE_1_0_G,
    KIOST_ESM,
    MCM_UA_1_0,
    MIROC6,
    MIROC6_ES2H,
    MIROC6_ES2L,
    MPI_ESM_1_2_HAM,
    MPI_ESM1_2_HR,
    MPI_ESM1_2_LR,
    MRI_ESM2_0,
    NESM3,
    NorCPM1,
    NorESM2_LM,
    NorESM2_MM,
    SAM0_UNICON,
    TaiESM1,
    UKESM1_0_LL,
]


models_per_institute = {}

for model in _all_models:
    inst = model.institute.acronym
    if not inst in models_per_institute.keys():
        models_per_institute[inst] = [model]
    else:
        models_per_institute[inst].append(model)

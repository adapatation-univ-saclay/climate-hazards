from enum import StrEnum as _StrEnum


class Experiment(_StrEnum):
    CMIP5 = "cmip5"
    CMIP6 = "cmip6"
    CORDEX = "cordex"

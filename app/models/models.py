from app.models.cmip6 import _all_models as _all_cmip6_models
from app.models.model import Model

all_models = _all_cmip6_models


__models_by_name = {model.flat_name(): model for model in all_models}
__models_by_id = {}


def parse_model(value: str | int | Model):
    global __models_by_id
    if len(__models_by_id) == 0:
        __models_by_id = {model.id: model for model in all_models}
    if isinstance(value, str):
        if value in __models_by_name.keys():
            return __models_by_name[value]
        else:
            raise ValueError(f"Unknown model name {repr(value)}")
    elif isinstance(value, int):
        if value in __models_by_id.keys():
            return __models_by_id[value]
        else:
            raise ValueError(f"Unknown model id {repr(value)}")
    elif isinstance(value, Model):
        return value
    else:
        raise TypeError(f"Cannot parse a model from a value of type {type(value)}")

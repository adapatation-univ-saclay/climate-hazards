import geocoder
from dash_extensions.enrich import DashProxy, DataclassTransform, Input, Output, State

import config
from app.indicators.dataframe import IndicatorDataFrame
from app.indicators.indicators import DAILY_AIR_TEMPERATURE
from app.locale import _
from app.plot.components import Layout, LocationDisplayContent
from app.plot.data import *
from app.plot.utils import *
from app.regions.geometry import Point
from app.scenarios.ssp import all_ssp_scenarios


def create_app(prefix: str | None = None):

    rpp = dict(requests_pathname_prefix=prefix) if prefix else {}

    app = DashProxy(
        __name__,
        transforms=[DataclassTransform()],
        title=_("interface.civ_app_title"),
        **rpp
    )

    indicator = DAILY_AIR_TEMPERATURE

    location = Point(lat=48.69572, lon=2.18727)

    MONTHS = MONTH_NAMES()

    def get_filtered_values(
        raw_values: list, month_num: int, scenario_names: list[str]
    ):
        all_values = indicator.make_df_from_raw(raw_values)
        scens = [SCENARIOS()[s] for s in scenario_names]

        return all_values.take_month(month_num).take_scenario(scens)

    app.layout = Layout(init_location=location)

    # Callback functions (for value updates)

    # Store

    @app.callback(
        Output("address-data", "data"),
        Input("submit-address", "n_clicks"),
        State("address-input", "value"),
    )
    def store_address(n_clicks, value):
        return geocode_address(value)

    @app.callback(
        Output(component_id="month-num", component_property="data"),
        Input(component_id="month-item", component_property="value"),
    )
    def update_month_number(month_chosen):
        month = MONTHS.index(month_chosen) + 1
        return month

    @app.callback(
        Output(component_id="scenario-names", component_property="data"),
        Input(component_id="scenario-item", component_property="value"),
    )
    def update_scenario_names(selected):
        return selected

    @app.callback(
        Output(component_id="unit-name", component_property="data"),
        Input(component_id="unit-item", component_property="value"),
    )
    def update_unit_name(col_chosen):
        return UNITS[col_chosen]

    # Components

    @app.callback(
        Output(component_id="location-display", component_property="children"),
        Input(component_id="address-data", component_property="data"),
    )
    def update_address_display(address: GeocodedResult):
        return LocationDisplayContent(
            address=address.address,
            city=address.city,
            postcode=address.postcode,
            latitude=address.lat,
            longitude=address.lon,
        )

    @app.callback(
        Output(component_id="raw-values", component_property="data"),
        Input(component_id="address-data", component_property="data"),
    )
    def update_value_with_location(address: GeocodedResult):
        new_values = indicator.get_values(
            location=Point(lat=address.lat, lon=address.lon), return_type="raw"
        )

        return new_values

    # Take input data to update figure

    @app.callback(
        Output(component_id="controls-and-graph", component_property="figure"),
        Input(component_id="month-num", component_property="data"),
        Input(component_id="scenario-names", component_property="data"),
        Input(component_id="unit-name", component_property="data"),
        Input(component_id="raw-values", component_property="data"),
    )
    def update_figure(month_num, scenario_names, unit_name, raw_values):
        fig = (
            get_filtered_values(raw_values, month_num, scenario_names)
            .convert(unit_name)
            .plot(to="dash")
        )

        return fig

    return app


# Run the app

if __name__ == "__main__":
    app = create_app()
    app.run(debug=True)

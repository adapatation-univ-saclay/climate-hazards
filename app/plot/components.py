from dash_extensions.enrich import dcc, html

from app.locale import _
from app.plot.data import DEFAULT_UNIT, MONTH_NAMES, SCENARIOS, UNITS
from app.plot.utils import longest_str_length

# Components


def HorizontalWrapper(children):
    return html.Div(
        children=children,
        style={"display": "flex", "flex-direction": "row", "width": "100%"},
    )


def Color(text: str, color: str):
    return html.Span(text, style={"color": color})


LocationInput = lambda: html.Div(
    children=[
        html.P(_("interface.campus_address")),
        dcc.Input(
            id="address-input",
            type="text",
            style={"width": "15 em"},
        ),
        html.Button(_("interface.submit"), id="submit-address", n_clicks=0),
    ]
)

Address = lambda x: html.Div(
    children=[
        html.Span(f'{_("interface.address.address")}:'),
        html.Span(x, id="display-address"),
    ]
)
City = lambda x: html.Div(
    children=[
        html.Span(f'{_("interface.address.city")}:'),
        html.Span(x, id="display-city"),
    ]
)
PostCode = lambda x: html.Div(
    children=[
        html.Span(f'{_("interface.address.postcode")}:'),
        html.Span(x, id="display-postcode"),
    ]
)
LatLon = lambda lat, lon: html.Div(
    children=[
        html.Span("Lat : "),
        html.Span(lat, id="display-latitude"),
        html.Span("Lon : "),
        html.Span(lon, id="display-longitude"),
    ]
)


def LocationDisplayContent(address, city, postcode, latitude, longitude):
    return [
        Address(address),
        City(city),
        PostCode(postcode),
        LatLon(latitude, longitude),
    ]


LocationDisplay = lambda init_location: html.Div(
    children=LocationDisplayContent("", "", "", init_location.lat, init_location.lon),
    id="location-display",
)

UnitSelector = lambda: html.Div(
    children=[
        html.P(_("interface.selection.unit")),
        dcc.Dropdown(
            options=list(UNITS.keys()),
            value=DEFAULT_UNIT,
            id="unit-item",
            style={"width": f"{longest_str_length(UNITS.keys()) + 2} em"},
        ),
    ],
)

MonthSelector = lambda: html.Div(
    children=[
        html.P(_("interface.selection.month")),
        dcc.Dropdown(
            options=MONTH_NAMES(),
            value=MONTH_NAMES()[0],
            id="month-item",
            style={"width": f"{longest_str_length(MONTH_NAMES()) + 2} em"},
        ),
    ],
)

ScenarioSelector = lambda: html.Div(
    children=[
        html.P(_("interface.selection.scenario")),
        dcc.Checklist(
            options=[
                {
                    "label": Color(text=name, color=scenario.color),
                    "value": name,
                }
                for name, scenario in SCENARIOS().items()
            ],
            value=[],
            id="scenario-item",
        ),
    ]
)

Title = lambda: html.H1(children=_("interface.civ_app_title"))

Graph = lambda: dcc.Graph(figure={}, id="controls-and-graph")


AddressStore = lambda: dcc.Store(id="address-data", storage_type="local")
MonthStore = lambda: dcc.Store(id="month-num", storage_type="local")
ScenarioStore = lambda: dcc.Store(id="scenario-names", storage_type="local")
UnitStore = lambda: dcc.Store(id="unit-name", storage_type="local")
RawValuesStore = lambda: dcc.Store(id="raw-values", storage_type="local")


Store = lambda: [
    AddressStore(),
    MonthStore(),
    ScenarioStore(),
    UnitStore(),
    RawValuesStore(),
]


Layout = lambda init_location: html.Div(
    [
        *Store(),
        Title(),
        html.Hr(),
        LocationInput(),
        LocationDisplay(init_location),
        HorizontalWrapper([UnitSelector(), MonthSelector(), ScenarioSelector()]),
        Graph(),
    ]
)

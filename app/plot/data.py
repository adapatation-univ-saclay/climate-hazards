from dataclasses import dataclass

from app.locale import _
from app.scenarios.ssp import all_ssp_scenarios

MONTH_NAMES = lambda: [
    _(f"interface.months.{m}")
    for m in [
        "january",
        "february",
        "march",
        "april",
        "may",
        "june",
        "july",
        "august",
        "september",
        "october",
        "november",
        "december",
    ]
]

SCENARIOS = lambda: {s.name: s for s in all_ssp_scenarios}


UNITS = {"Kelvin": "kelvin", "°C": "celsius", "°F": "fahrenheit"}
DEFAULT_UNIT = "°C"


@dataclass
class GeocodedResult:
    address: str = ""
    city: str = ""
    postcode: str = ""
    lat: float = 0.0
    lon: float = 0.0

from dataclasses import dataclass

import geocoder


@dataclass
class GeocodedResult:
    address: str = ""
    city: str = ""
    postcode: str = ""
    lat: float = 0.0
    lon: float = 0.0


def longest_str_length(l: list[str]) -> int:
    return max(len(s) for s in l)


def geocode_address(address: str):

    if address and len(address) != 0:

        loc = geocoder.osm(address)

        if hasattr(loc, "lng") and hasattr(loc, "lat"):
            return GeocodedResult(
                address=loc.current_result.json["raw"]["name"],
                city=loc.current_result.city,
                postcode=loc.current_result.postal,
                lat=loc.lat,
                lon=loc.lng,
            )
    return GeocodedResult("", "", "", lat=location.lat, lon=location.lon)

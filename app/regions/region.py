from dataclasses import dataclass

from pyproj.aoi import BBox as _BBox


@dataclass
class Region:
    name: str
    bbox: _BBox | None

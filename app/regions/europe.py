from pyproj.aoi import BBox as _BBox

from app.regions.region import Region

FRANCE = Region("France", _BBox(-9.86, 41.15, 10.38, 51.56))


EUROPEAN_REGIONS = [FRANCE]

from __future__ import annotations

import math
from collections.abc import Sequence as _Sequence

from numpy import argmin


class Point:
    __match_args__ = ("lat", "lon")

    def __init__(self,
        lat : int | float | None = None,
        lon : int | float | None = None,
        x: int | float | None = None,
        y: int | float | None = None,
    ):
        if lat is not None and lon is not None:
            self.lat = lat
            self.lon = lon
        elif x is not None and y is not None:        
            self.lat = y
            self.lon = x

    @staticmethod
    def parse(value: tuple | list | dict | Point) -> Point:
        match value:
            case Point(lat=lat, lon=lon):
                return Point(lon=lon, lat=lat)
            case tuple() | list():
                if len(value) != 2:
                    raise ValueError(
                        "Cannot parse a point from a list/tuple of len != 2"
                    )
                else:
                    return Point(value[0], value[1])
            case {"x": x, "y": y} | {"lat": y, "lon": x}:
                return Point(x, y)

    @staticmethod
    def bulk_parse(seq: _Sequence[tuple | list | dict | Point]) -> list[Point]:
        acc = []
        for s in seq:
            acc.append(Point.parse(s))
        return acc

    def norm(self) -> float:
        return math.sqrt(self.lat**2 + self.lon**2)

    def __add__(self, other) -> Point:
        match other:
            case Point(lat=lat, lon=lon):
                return Point(lat=self.lat + lat, lon=self.lon + lon)
            case int() | float():
                return Point(lat=self.lat + other, lon=self.lon + other)
            case _:
                raise TypeError(f"Cannot add object of type {type(other)} to a Point")

    def __radd__(self, other) -> Point:
        return self.__add__(other)

    def __neg__(self) -> Point:
        return Point(lat=-self.lat, lon=-self.lon)

    def __sub__(self, other) -> Point:
        return self + (-other)

    def __mul__(self, other) -> Point | int | float:
        match other:
            case Point(lat=lat, lon=lon):
                return self.lat * lat + self.lon * lon
            case int() | float():
                return Point(self.lat * other, self.lon * other)
            case _:
                raise TypeError(
                    f"Cannot multiply a Point by an object of type {type(other)}"
                )

    def __rmul__(self, other) -> Point | int | float:
        return self.__mul__(other)

    def find_closest(self, seq: _Sequence[Point]) -> Point:
        """Finds the closest Point to this Point in a given Sequence.

        Args:
            seq: the sequence in which the closest Point should be found
        """

        idx = argmin([(point - self).norm() for point in seq])
        closest_point = seq[idx]
        return closest_point

    def __str__(self) -> str:
        return f"Point(lat={self.lat}, lon={self.lon})"

    def __repr__(self) -> str:
        return self.__str__()

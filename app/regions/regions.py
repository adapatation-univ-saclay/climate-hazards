from app.regions.europe import EUROPEAN_REGIONS

all_regions = {region.name: region for region in EUROPEAN_REGIONS}


def parse_region(region: str):
    if region in all_regions.keys():
        return region
    else:
        raise ValueError(f"Unknown region {repr(region)}")

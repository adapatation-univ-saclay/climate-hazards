from pint import Unit

from app.units import ureg

NAMES = {
    "kelvin": "Kelvin",
    "degree_Celsius": "°C",
    "degree_Fahrenheit": "°F",
}


def friendly_name(unit: Unit):
    unfriendly_name = str(unit)
    unit = ureg.Unit(unfriendly_name)
    real_name = str(unit)
    return NAMES.get(real_name)

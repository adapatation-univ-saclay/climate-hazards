from typing import Literal

from app.units import ureg as __ureg

kilometer_per_hour = __ureg.kilometer_per_hour
kph = kilometer_per_hour

meter_per_second = __ureg.meter_per_second
mps = __ureg.meter_per_second

Speed = Literal[kilometer_per_hour, meter_per_second]

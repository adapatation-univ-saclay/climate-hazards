from typing import Literal

from app.units import ureg as __ureg

second = __ureg.second
s = second

minute = __ureg.minute
mi = minute

hour = __ureg.hour
h = __ureg.hour

day = __ureg.day
d = __ureg.day

month = __ureg.month
mo = month

year = __ureg.year
yr = year


Duration = Literal[second, minute, hour, day, month, year]

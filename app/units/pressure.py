from typing import Literal

from app.units import ureg as __ureg

pascal = __ureg.pascal
Pa = pascal

hectopascal = __ureg.hectopascal
hPa = hectopascal

bar = __ureg.bar

Pressure = Literal[pascal, hectopascal, bar]

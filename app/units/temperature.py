from typing import Literal

from app.units import ureg as __ureg

celsius = __ureg.degreeC
Celsius = __ureg.degreeC
C = celsius

farenheit = __ureg.degreeF
Farenheit = __ureg.degreeF
F = farenheit

kelvin = __ureg.kelvin
Kelvin = kelvin
K = kelvin

Temperature = Literal[celsius, farenheit, kelvin]

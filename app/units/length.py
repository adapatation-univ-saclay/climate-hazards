from typing import Literal

from app.units import ureg as __ureg

millimeter = __ureg.millimeter
mm = millimeter

centimeter = __ureg.centimeter
cm = centimeter

meter = __ureg.meter
m = meter

kilometer = __ureg.kilometer
km = kilometer


Length = Literal[millimeter, centimeter, meter, kilometer]

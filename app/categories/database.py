from app.categories.categories import all_categories
from app.database.handler import DBHandler

SCHEMA = "hazard"
TABLE = "categories"


def insert_all():
    categories = [(k,) for k in all_categories.keys()]
    with DBHandler() as db:
        db.upsert_silent(
            schema=SCHEMA,
            table=TABLE,
            columns=["name"],
            values=categories,
            conflict_on="name",
        )


def set_ids():
    with DBHandler() as db:
        ids = {
            name: idx
            for (idx, name) in db.fetch_all(f"SELECT id, name FROM {SCHEMA}.{TABLE};")
        }

        for category in all_categories.values():
            name = category.abbreviation
            idx = ids[name]
            category.id = idx


def init():
    insert_all()
    set_ids()

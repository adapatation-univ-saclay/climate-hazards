from app.hazards.hazard import Hazard as _Hazard
from app.locale import _
from app.serialize import _next_level


class Category:
    def __init__(self, abbreviation: str, hazards: list[_Hazard] = None):
        self.abbreviation = abbreviation
        self.hazards = hazards

    def name(self) -> str:
        """Return the full, localized name of the category."""
        return _(f"hazards.{self.abbreviation}.name")

    @property
    def id(self) -> int:
        """The category id in database"""
        return self._id

    @id.setter
    def id(self, id: int):
        self._id = id

    def serialize(
        self, depth: int = 1, base_url: str | None = None
    ) -> dict[str, str | list]:
        """Serialize the category into a dictionary.

        Args:
            depth: the depth of serialization for nested objects, defaults to 1
            base_url: the base api URL, defaults to None. If not None, will provide
                the objects' api URL

        Returns:
            the category, serialized as a dictionary.
        """
        s = {
            "name": self.name(),
            "abbreviation": self.abbreviation,
            "hazards": [
                (
                    h.serialize(depth=_next_level(depth), base_url=base_url)
                    if depth
                    else h.abbreviation
                )
                for h in self.hazards
            ],
        }

        if base_url:
            s["url"] = f"{base_url}categories/{self.abbreviation}"
        return s

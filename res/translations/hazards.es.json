{
    "es": {
        "heat_and_cold": {
            "name": "Calor y frío",
            "mean_air_temperature": {
                "name": "Temperatura media del aire",
                "description": "Temperatura media del aire en superficie y sus ciclos diurnos y estacionales."
            },
            "extreme_heat": {
                "name": "Calor extremo",
                "description": "Eventos episódicos de alta temperatura del aire en superficie potencialmente exacerbados por la humedad."
            },
            "cold_spell": {
                "name": "Oleada de frío",
                "description": "Eventos episódicos de temperatura fría del aire en superficie potencialmente exacerbados por el viento."
            },
            "frost": {
                "name": "Frost",
                "description": "Eventos de congelación y deshielo cerca de la superficie terrestre y su estacionalidad"
            }
        },
        "wet_and_dry": {
            "name": "Humedad y sequía",
            "precipitación_media": {
                "name": "Precipitación media",
                "description": "Precipitación media y sus ciclos diurnos y estacionales."
            },
            "river_flood": {
                "name": "Crecida del río",
                "description": "Altos niveles episódicos de agua en arroyos y ríos impulsados por la escorrentía de la cuenca y el ciclo estacional previsto de inundaciones"
            },
            "heavy_precipitation": {
                "name": "Precipitación intensa e inundación pluvial",
                "description": "Altas tasas de precipitación y la consiguiente inundación episódica y localizada de arroyos y tierras llanas."
            },
            "landslide": {
                "nombre": "Deslizamiento de tierra",
                "description": "Condiciones del terreno y atmosféricas que dan lugar a movimientos geológicos de masas, incluidos el corrimiento de tierras, el desprendimiento de lodo y la caída de rocas."
            },
            "aridity": {
                "nombre": "Aridez",
                "descripción": "Condiciones medias de precipitación y evapotranspiración en comparación con la demanda potencial de agua atmosférica y superficial, lo que da lugar a una baja media de agua superficial, baja humedad del suelo y/o baja humedad relativa."
            },
            "hydrological_drought": {
                "name": "sequía hidrológica",
                "description": "Combinación episódica de déficit de escorrentía y demanda evaporativa que afecta a la disponibilidad de aguas superficiales o subterráneas."
            },
            "agricultural_drought": {
                "name": "Sequía agrícola y ecológica",
                "description": "Combinación episódica de déficit de suministro de humedad del suelo y requisitos de demanda atmosférica que desafía la capacidad de la vegetación para satisfacer sus necesidades de agua para la transpiración y el crecimiento.Nota: el término 'agrícola' frente a 'ecológico' depende del bioma afectado."
            },
            "fire_weather": {
                "name": "Clima de incendio",
                "description": "Condiciones meteorológicas propicias para desencadenar y mantener incendios forestales, normalmente basadas en un conjunto de indicadores y combinaciones de indicadores que incluyen la temperatura, la humedad del suelo, la humedad y el viento. El tiempo de incendio no incluye la presencia o ausencia de carga de combustible. Nota: distinto de la ocurrencia de incendios forestales y del área quemada."
            }
        },
        "wind": {
            "name": "Viento",
            "mean_wind_speed": {
                "name": "Velocidad media del viento",
                "description": "Velocidad media del viento y patrones de transporte y sus ciclos diurnos y estacionales."
            },
            "severe_wind_storm": {
                "name": "Tormenta de viento severa",
                "description": "Tormentas severas episódicas que incluyen tormentas de viento de ciclones extratropicales, tormentas eléctricas, ráfagas de viento, derechos y tornados."
            },
            "tropical_cyclone": {
                "name": "Ciclón tropical",
                "description": "Tormenta fuerte y giratoria que se origina sobre océanos tropicales acompañada de fuertes vientos, precipitaciones y mareas tormentosas."
            },
            "sand_dust_storm": {
                "name": "Tormenta de arena y polvo",
                "description": "Tormentas que provocan el transporte de tierra y partículas finas de polvo."
            }
        },
        "snow_and_ice": {
            "name": "Nieve y hielo",
            "snow_glacier_sheet": {
                "name": "Nieve, glaciar y capa de hielo",
                "description": "Estacionalidad del manto de nieve y características de los glaciares y las capas de hielo, incluidos los eventos de parto y el agua de deshielo."
            },
            "permafrost": {
                "name": "Permafrost",
                "description": "Capas de suelo profundas permanentemente congeladas, sus características de hielo y las características de los suelos estacionalmente congelados por encima."
            },
            "lake_river_sea": {
                "name": "Hielo de lago, río y mar",
                "description": "Estacionalidad y características de las formaciones de hielo en el océano y en las masas de agua dulce."
            },
            "heavy_snowfall": {
                "name": "Fuerte nevada y tormenta de hielo",
                "description": "Eventos de fuertes nevadas y tormentas de hielo que incluyen condiciones de lluvia helada y lluvia sobre nieve."
            },
            "hail": {
                "name": "Granizo",
                "descripción": "Tormentas que producen granizo sólido."
            },
            "snow_avalanche": {
                "name": "Avalancha de nieve",
                "description": "Movimientos de masas criosféricas y las condiciones de colapso del manto de nieve."
            }
        },
        "coastal": {
            "name": "Riesgos costeros",
            "relative_sea_level": {
                "name": "Nivel relativo del mar",
                "description": "La altura media local de la superficie del mar relativa a la superficie sólida local."
            },
            "coastal_flood": {
                "name": "Inundación costera",
                "description": "Inundación provocada por episodios de altos niveles de agua costera que resultan de una combinación de aumento relativo del nivel del mar, mareas, mareas de tormenta y configuración de olas."
            },
            "coastal_erosion": {
                "name": "Erosión costera",
                "description": "Cambio a largo plazo o episódico en la posición de la línea de costa causado por el aumento relativo del nivel del mar, las corrientes cercanas a la costa, las olas y las mareas de tempestad."
            }
        },
        "ocean": {
            "name": "Océano",
            "mean_ocean_temperature": {
                "name": "Temperatura media del océano",
                "description": "Perfil de temperatura media del océano a través de las estaciones, incluyendo el contenido de calor a diferentes profundidades y la estratificación asociada."
            },
            "marine_heatwave": {
                "name": "Ola de calor marina",
                "description": "Temperaturas oceánicas extremas episódicas."
            },
            "ocean_acidity": {
                "name": "Acidez oceánica",
                "description": "Perfil de los niveles de pH del agua oceánica y concentraciones acompañantes de iones carbonato y bicarbonato."
            },
            "ocean_salinity": {
                "name": "Salinidad del océano",
                "description": "Perfil de salinidad oceánica y estratificación estacional asociada.Nota: distinto de la salinización de los recursos de agua dulce."
            },
            "dissolved_oxygen": {
                "name": "Oxígeno disuelto",
                "description": "Perfil del oxígeno disuelto en el agua del océano y eventos episódicos de bajo oxígeno."
            }
        },
        "other": {
            "name": "Otros riesgos",
            "air_pollution_weather": {
                "name": "Tiempo de contaminación atmosférica",
                "descripción": "Condiciones atmosféricas que aumentan la probabilidad de que se produzcan altas concentraciones de partículas u ozono o procesos químicos generadores de contaminantes atmosféricos.Nota: distinto de las emisiones de aerosoles o de las concentraciones de contaminación atmosférica propiamente dichas."
            },
            "co2_at_surface": {
                "name": "CO₂ atmosférico en superficie",
                "description": "Otros CO₂ atmosférico Concentración de dióxido de carbono atmosférico (CO₂) en la superficie. Nota: distinto del efecto radiativo global del CO₂ como gas de efecto invernadero."
            },
            "radiation_at_surface": {
                "name": "Radiación en superficie",
                "description": "Balance de la radiación neta de onda corta, onda larga y ultravioleta en la superficie de la Tierra y sus patrones diurnos y estacionales."
            }
        }
    }
}
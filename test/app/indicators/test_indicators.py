import app.indicators.indicator


from app.indicators.indicator import Indicator
from app.models import cmip6
from app.regions.europe import FRANCE
from app.scenarios.ssp import SSP2_4_5
from app.time.period import Day, Decade


def test_make_indicator_download_request():
    variable_name = "daily_maximum_near_surface_air_temperature"
    daily_air_temperature = Indicator(
        name="Daily surface temperature",
        variable_name=variable_name,
        temporal_resolution=Day,
    )
    req = daily_air_temperature.make_download_request_dict(
        scenario=SSP2_4_5, region=FRANCE, period=Decade(2010), model=cmip6.CNRM_CM6_1
    )
    assert req == {
        "experiment": "ssp2_4_5",
        "area": [51.56, -9.86, 41.15, 10.38],
        "variable": variable_name,
        "temporal_resolution": "daily",
        "year": [
            "2010",
            "2011",
            "2012",
            "2013",
            "2014",
            "2015",
            "2016",
            "2017",
            "2018",
            "2019",
        ],
        "month": [
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
        ],
        "day": [
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31",
        ],
        "format": "zip",
    }

# climate-hazards

Application permettant de visualiser les aléas climatiques à l'échelle d'un campus sur différents horizons de temps (jusqu'à 2100), et différents horizons d'émissions de CO2 (RCP2.6, 4.5 et 8.5).

# Installation

## Docker

Ce projet utilise docker. Suivez les [instructions d'installation](https://docs.docker.com/engine/install/) spécifiques à votre plateforme.

## Utilisation

Pour démarrer le projet, utilisez la commande suivante :

    docker compose up


# Développement

Pour développer localement en python, suivez les instructions suivantes :

## Version de python

Nous recommandons l'utilsation de [pyenv](https://github.com/pyenv/pyenv) pour gérer vos versions de python. Ce projet a été développé pour fonctionner avec **python 3.11**.

## Environnement virtuel

Créez un environnement virtuel à l'aide de la commande suivante :

    python -m venv .env

Puis activez le :

    source .env/bin/activate

sur Unix/Linux/macOS/windows avec WSL, ou :

    venv\Scripts\activate

sur Windows (sans WSL).

## Dépendances

Une fois votre environnement virtuel activé, installez les dépendances avec pip :

    pip install -r requirements.txt
CREATE SCHEMA IF NOT EXISTS hazard;

CREATE TABLE IF NOT EXISTS hazard.categories (
    id int PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    name VARCHAR,
    CONSTRAINT hazard_categories_unique_name UNIQUE (name)
);

CREATE TABLE IF NOT EXISTS hazard.hazards (
    id int PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    name VARCHAR,
    category_id int,
    CONSTRAINT hazard_hazards_unique_name UNIQUE (name)
);


CREATE SCHEMA IF NOT EXISTS scenario;

CREATE TABLE IF NOT EXISTS scenario.scenarios (
    id int PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    name VARCHAR,
    scenario_type VARCHAR,
    radiative_forcing double precision,
    socio_economic_type VARCHAR,
    CONSTRAINT scenario_scenarios_unique_name UNIQUE (name)
);


CREATE SCHEMA IF NOT EXISTS model;

CREATE TABLE IF NOT EXISTS model.institutes (
    id int PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    name VARCHAR,
    acronym VARCHAR,
    country VARCHAR,
    CONSTRAINT model_institutes_unique_name UNIQUE (name)
);

CREATE TABLE IF NOT EXISTS model.models (
    id int PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    name VARCHAR,
    flat_name VARCHAR,
    experiment VARCHAR,
    institute_id int,
    CONSTRAINT model_models_unique_name UNIQUE (name)
);


CREATE SCHEMA IF NOT EXISTS indicator;

CREATE TABLE IF NOT EXISTS indicator.datasets (
    id int PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    name VARCHAR,
    available_periods jsonb,
    CONSTRAINT indicator_datasets_unique_name UNIQUE (name)
);


CREATE TABLE IF NOT EXISTS indicator.indicators (
    id int PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    name VARCHAR,
    hazard_id int,
    dataset_id int,
    variable_name VARCHAR,
    temporal_resolution VARCHAR,
    unit VARCHAR,
    CONSTRAINT indicator_indicators_unique_name UNIQUE (name)
);

CREATE TABLE IF NOT EXISTS indicator.values (
    indicator_id int,
    scenario_id int,
    model_id int,
    geom geometry(Point, 4326),
    timestp timestamp,
    val double precision
);


CREATE TABLE IF NOT EXISTS indicator.monthly_values (
    indicator_id int,
    scenario_id int,
    model_id int,
    geom geometry(Point, 4326),
    timestp timestamp,
    val double precision
);

source .venv/bin/activate
git clone https://gitlab-student.centralesupelec.fr/adapatation-univ-saclay/nc-pgstore.git
cd nc-pgstore

pip install maturin

source "$HOME/.cargo/env"
maturin develop
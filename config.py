from dataclasses import dataclass

ENV_PATH = ".env"
BASE_DATA_DIR = "data/"


def load_env(path: str = ENV_PATH) -> dict:
    variables = {}
    with open(path, "r") as env_file:
        for line in env_file.readlines():
            splt = line.replace("\n", "").split("=")
            if len(splt) == 2:
                name, value = splt
                variables[name] = value
    return variables


ENV = load_env()


@dataclass
class DatabaseConfig:
    host: str
    database: str
    username: str
    password: str
    port: int = 5432


DATABASE = DatabaseConfig(
    host=ENV["POSTGRES_HOST"],
    database=ENV["POSTGRES_DB"],
    username=ENV["POSTGRES_USER"],
    password=ENV["POSTGRES_PASSWORD"],
)


@dataclass
class DashConfig:
    ip: str
    port: int
    prefix: int
    workers: int = 1


ENVIRONMENT = "production"
DASH = DashConfig(ip="0.0.0.0", port=8050, prefix="plot", workers=2)

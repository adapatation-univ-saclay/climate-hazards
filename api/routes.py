import i18n as i18n
from fastapi import APIRouter, FastAPI, Request
from fastapi.middleware.wsgi import WSGIMiddleware

import config
from api.routers import categories, hazards, models
from app.locale import set_locale
from app.plot.app import create_app

routes = APIRouter()

routes.include_router(categories.router)
routes.include_router(hazards.router)
routes.include_router(models.router)


# Localization
locales = ["en", "fr", "es"]


def define_localized_api(locale: str) -> FastAPI:
    """Define a localized sub router with a middleware function that explicitely
    sets i18n locale.

    Args:
        locale: the locale
    """
    localized_api = FastAPI()
    localized_api.include_router(routes)

    @localized_api.middleware("http")
    async def set_locale(request: Request, call_next):
        i18n.set("locale", locale)
        set_locale(locale)
        response = await call_next(request)
        return response

    dash_app = create_app()

    localized_api.mount(
        f"/{config.DASH.prefix}",
        WSGIMiddleware(app=create_app(f"/{locale}/{config.DASH.prefix}/").server),
    )

    return localized_api


api = FastAPI()

api.include_router(routes)


@api.middleware("http")
async def set_base(request: Request, call_next):
    response = await call_next(request)
    return response


for locale in locales:
    loc_api = define_localized_api(locale)
    api.mount(f"/{locale}", loc_api)

app = FastAPI()
app.mount(
    f"/{config.DASH.prefix}",
    WSGIMiddleware(app=create_app(f"/{config.DASH.prefix}/").server),
)
app.mount("/", api)

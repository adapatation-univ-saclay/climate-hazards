from fastapi import Request

locales = ["en", "fr", "es"]


def base_url(request: Request) -> str:
    if len(request.url.path) >= 4:
        for locale in locales:
            if f"/{locale}/" == request.url.path[:4]:
                return f"{request.base_url}{locale}/"
    return request.base_url

from fastapi import APIRouter, Request

from api.urls import base_url
from app.categories.category import Category
from app.categories.categories import all_categories
from app.serialize import serialize


router = APIRouter(prefix="/categories")


@router.get(f"/")
def categories(request: Request):
    return {
        "categories": serialize(all_categories, depth=0, base_url=base_url(request))
    }


@router.get("/{category_id}")
def category(category_id: str, request: Request):
    category = all_categories[category_id]
    return category.serialize(base_url=base_url(request))

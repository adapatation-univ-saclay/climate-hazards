from fastapi import APIRouter, Request

from api.urls import base_url
from app.hazards import all_hazards
from app.serialize import serialize

router = APIRouter(prefix="/hazards")


@router.get(f"/")
def hazards(request: Request):
    return {"hazards": serialize(all_hazards, depth=0, base_url=base_url(request))}


@router.get("/{hazard_id}")
def hazard(hazard_id: str, request: Request):
    haz = all_hazards[hazard_id]
    return haz.serialize(depth=0, base_url=base_url(request))

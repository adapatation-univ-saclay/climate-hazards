from fastapi import APIRouter, Request

from api.urls import base_url
from app.models import Model
from app.models.cmip6 import _all_models as cmip6_all_models
from app.models.cmip6 import models_per_institute as cmip6_per_insitute
from app.models.institutes import _all_institutes as all_institutes
from app.serialize import serialize

all_models = {model.flat_name(): model for model in cmip6_all_models}
all_models_by_experiment = {"cmip6": cmip6_all_models}

router = APIRouter(prefix="/models")


@router.get(f"/")
def models(request: Request):
    return {"models": serialize(all_models, depth=0, base_url=base_url(request))}


@router.get("/experiments/{experiment}")
def models_by_experiment(experiment, request: Request):
    models = all_models_by_experiment[experiment]
    return serialize(models, base_url=base_url(request))


@router.get("/institutes")
def institutes(request: Request):
    return serialize(all_institutes, base_url=base_url(request))


@router.get("/random")
def one_random_per_institute():
    return [inst.get_random_model() for inst in all_institutes]


@router.get("/{model_id}")
def model(model_id: str, request: Request):
    model = all_models[model_id.lower()]
    return model.serialize(base_url=base_url(request))
